import React from "react";
import ReactDOM from "react-dom";
import App from "./components/app/App";
import * as serviceWorker from "./serviceWorker";
import * as firebase from "firebase/app";

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

let app;

export const Initialize = () => {
  const firebaseConfig = {
    apiKey: "AIzaSyBb7d-rT2zXhB3uScBLCr9BLSqQo3vT5c0",
    authDomain: "gosolotrip-backend.firebaseapp.com",
    databaseURL: "https://gosolotrip-backend.firebaseio.com",
    projectId: "gosolotrip-backend",
    storageBucket: "gosolotrip-backend.appspot.com",
    messagingSenderId: "901986711402",
    appId: "1:901986711402:web:d0b2a81a0c435017c39713",
    measurementId: "G-SP0DDPFN69"
  };

  app = firebase.initializeApp(firebaseConfig);
};

export const isAppLoaded = () => !!app;

ReactDOM.render(<App />, document.getElementById("root"));
