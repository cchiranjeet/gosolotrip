import * as firebase from "firebase";
import "firebase/database";

import { isAppLoaded, Initialize } from "../index";

export function get(query) {
  if (!isAppLoaded()) {
    Initialize();
  }

  const db = firebase.database();
  return db
    .ref(query)
    .once("value")
    .then(snapshot => snapshot.val())
    .catch(error => console.log(error));
}
