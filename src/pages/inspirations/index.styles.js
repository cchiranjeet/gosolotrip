import styled from "styled-components";

export const SoloTripContainer = styled.div`
  margin-top: 0;
`;
export const SoloMain = styled.div`
  margin-top: 50px;
  @media (max-width: 768px) {
    margin-top: 20px;
  }
`;
export const TripSection = styled.div`
  margin-bottom: 50px;
  @media (max-width: 768px) {
    margin-bottom: 30px;
  }
`;
export const Loader = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  width: 100%;
  min-height: 600px;
`;
