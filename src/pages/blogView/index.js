/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-script-url */
import React from "react";
import { ReactComponent as RightArrow } from "../../assets/img/right-arrow.svg";
import { withRouter } from "react-router-dom";
import { Container, Row, Hidden } from "react-grid-system";
import AddBanner from "../../assets/img/add.jpeg";

//components
import Cover from "../../components/home/cover2";
import Gallery from "../../components/gallery";
import StickysideBar from "../../components/blog/StickySideBar";
import HeaderContent from "../../components/blog/headerContent";
import DestinationTile from "../../components/blog/destinationTile";
import RelatedPost from "../../components/blog/relatedPost";
import ResentStories from "../../components/blog/recentStories";
import GuidHeading from "../../components/blog/guidHeading";
import Itinerary from "../../components/blog/itinerary";
import Heading from "../../components/heading";
import Pargraph from "../../components/blog/paragraph";
import ParagraphHeading from "../../components/blog/paragraphHeading";
import ParagraphList from "../../components/blog/paragraphList";
import Tabs from "../../components/tab/tabs";

//styled
import * as styles from "./index.style";

// Utils
import * as DB from "../../utils/db";

class BlogView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      blog: null
    };
  }

  componentDidMount() {
    this.props.changeHeaderTheme(false);
    window.scroll(0, 0);
    DB.get(`/blogs/${this.props.match.params.id}`).then(data => {
      this.setState({ blog: data });
      if (window.FB) {
        window.FB.XFBML.parse();
      }
    });
  }

  render() {
    if (!this.state.blog) {
      return <styles.Loader>Loading...</styles.Loader>;
    }

    const { blog } = this.state;
    return (
      <>
        <styles.SoloTripContainer>
          <styles.SoloMain>
            <styles.TripSection>
              <Cover
                imgUrl={blog.content.imgUrl}
                heading={blog.content.heading}
                subheading={blog.content.subheading}
              />
            </styles.TripSection>
          </styles.SoloMain>
          <styles.SoloMain>
            <styles.BlogSection>
              <Container fluid style={{ maxWidth: "1200px" }}>
                <Row>
                  <Hidden xs sm>
                    <styles.ColWithPadding md={2}>
                      <StickysideBar />
                    </styles.ColWithPadding>
                  </Hidden>
                  <styles.ColWithPaddingMiddle md={6.5} xs={12} sm={12}>
                    <HeaderContent content={blog.content} />
                    <Pargraph content={blog.content.para1} />
                    <Pargraph content={blog.content.para2} />

                    <Gallery galleryImg={blog.content.galleryImg} />

                    <Pargraph content={blog.content.para3} />

                    <ParagraphHeading
                      text={"Best time to visit " + blog.location_name}
                    />
                    <Pargraph content={blog.content.para4} />
                    <ParagraphHeading text="Things to carry" />
                    <ParagraphList content={blog.content.listSectionFirst} />
                    <ParagraphHeading text="Best places to Stay" />
                    <ParagraphList content={blog.content.listSectionSecond} />

                    <Tabs>
                      <div label="Overview">
                        <Pargraph content={blog.content.para2} />
                        <div className="rotated">
                          Itinerary at a Glance{" "}
                          <span className="view">
                            View Detailed Itinerary <RightArrow />
                          </span>
                        </div>
                        <Itinerary />
                      </div>
                      <div label="Itinerary">
                        {blog.Guide.map((item, index) => {
                          return (
                            <div key={index}>
                              <GuidHeading title={item.heading} />
                              <Pargraph content={item.para} />
                              <Gallery galleryImg={item.galleryImg} />
                            </div>
                          );
                        })}
                      </div>
                      <div label="Go Solo Trip">Go Solo at japur</div>
                      <div label="Accommodation">
                        Nothing to see here, this tab is <em>extinct</em>!
                      </div>
                    </Tabs>
                    <Heading>RELATED POSTS</Heading>
                    <RelatedPost />
                    {/* <Heading>LEAVE A REPLY</Heading> */}
                    {/* <CommentForm /> */}
                    <Heading> 5 COMMENTS</Heading>
                    <div
                      class="fb-comments"
                      data-href="https://developers.facebook.com/docs/plugins/comments#configurator"
                      data-width=""
                      data-numposts="5"
                    ></div>
                    {/* <Comment /> */}
                  </styles.ColWithPaddingMiddle>
                  <Hidden xs sm>
                    <styles.ColWithPadding md={3.5} xs={12} sm={12}>
                      <styles.RightContainer>
                        <DestinationTile />
                        <ResentStories />
                      </styles.RightContainer>
                      <styles.Addbanner>
                        <img src={AddBanner} alt="go solo trip" />
                      </styles.Addbanner>
                    </styles.ColWithPadding>
                  </Hidden>
                </Row>
              </Container>
            </styles.BlogSection>
          </styles.SoloMain>
        </styles.SoloTripContainer>
      </>
    );
  }
}

export default withRouter(BlogView);
