import styled from "styled-components";
import { Col } from "react-grid-system";

export const SoloTripContainer = styled.div`
  margin-top: 0px;
  position: relative;
`;
export const SoloMain = styled.div``;
export const TripSection = styled.div``;
export const BlogSection = styled.div`
  position: relative;
  background: linear-gradient(to right, white 50%, #f5f5f5 50%);
`;
export const RightContainer = styled.div`
  background: #f5f5f5;
  width: calc(100%);
  background: #f5f5f5;
  padding-left: 35px;
  text-align: right;
  @media (max-width: 768px) {
    padding-left: 15px;
  }
`;

export const ColWithPadding = styled(Col)`
  padding-top: 50px;
  @media (max-width: 768px) {
    padding-top: 20px;
  }
`;
export const Addbanner = styled.div`
  position: sticky;
  top: 100px;
  background: #f5f5f5;
  padding-left: 35px;
  padding-bottom: 50px;
  text-align: right;
`;
export const ColWithPaddingMiddle = styled(ColWithPadding)`
  background: white;
  padding-right: 40px !important;
  padding-bottom: 50px;
  @media (max-width: 768px) {
    padding-right: unset;
    padding-bottom: 30px;
  }
`;
export const Loader = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  width: 100%;
  min-height: 600px;
`;
