import React from "react";
import { Container, Row, Col } from "react-grid-system";

//components
import Map from "../../components/map";
import BlogHeader from "../../components/blogHeading";
import TravelPost from "../../components/blog/BlogTile";

// styles
import * as styles from "./index.styles";

// Utils
import * as DB from "../../utils/db";

class Destinations extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      destinations: null,
      filteredValues: []
    };
  }

  getDataFromFirebase = () => {
    DB.get("/catogories").then(data => {
      this.setState({ destinations: data });
    });
  };

  toBlogView = id => {
    localStorage.setItem("greyHeader", 1);
    this.props.history.push({
      pathname: `/explore/${this.props.match.params.category}/${id}`
    });
  };

  componentDidMount() {
    console.log(this.props);
    window.scrollTo(0, 0);
    this.getDataFromFirebase();
    this.props.changeHeaderTheme(true);
  }

  render() {
    if (!this.state.destinations) {
      return <styles.Loader>Loading...</styles.Loader>;
    }

    const currentDestination = this.state.destinations[
      this.props.match.params.category
    ];
    return (
      <>
        <styles.SoloTripContainer>
          <Map
            zoom={7}
            initialCenter={{ lat: 27.0238, lng: 74.2179 }}
            markers={[
              {
                name: "Jaisalmer",
                position: { lat: 26.9157, lng: 70.9083 }
              },
              {
                name: "Udaipur",
                position: { lat: 24.5854, lng: 73.7125 }
              },
              {
                name: "Jaipur",
                position: { lat: 26.9124, lng: 75.7873 }
              }
            ]}
          />

          <styles.SoloMain>
            <styles.TripSection>
              <Container fluid style={{ maxWidth: "1200px" }}>
                <Row>
                  <Col md={12}>
                    <BlogHeader
                      headingtxt={currentDestination.title}
                      articleno="8 articles"
                      paragraph={currentDestination.subtitle}
                    />
                  </Col>
                </Row>
                <Row>
                  {currentDestination.blogs.map((item, index) => (
                    <Col md={6} key={index}>
                      <TravelPost
                        toBlogView={() => this.toBlogView(item.id)}
                        location={item.title}
                        url=""
                        imagesrc={item.imgUrl} // item.imgUrl
                        title={item.heading}
                        lastdateofpost="6 days ago"
                        comment="Add comment"
                        content={item.mini_content}
                      />
                    </Col>
                  ))}
                </Row>
              </Container>
            </styles.TripSection>
          </styles.SoloMain>
        </styles.SoloTripContainer>
      </>
    );
  }
}

export default Destinations;
