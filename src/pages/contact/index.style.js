import styled from "styled-components";

export const SoloTripContainer = styled.div``;
export const HeadingContent = styled.h1`
  text-align: center;
  width: 100%;
  font-weight: 500;
`;

export const SoloMain = styled.div`
  margin-top: 70px;
  @media (max-width: 768px) {
    margin-top: 30px;
  }
`;
export const TripSection = styled.div`
  margin-bottom: 70px;
  @media (max-width: 768px) {
    margin-bottom: 30px;
  }
`;
export const Paragraph = styled.p`
  margin-top: 0;
  margin-bottom: 3rem;
  font-weight: 500;
  text-align: center;
  @media (max-width: 768px) {
    margin-bottom: 1rem;
    font-size: 0.9rem;
    &:first-child {
      margin-top: 1rem;
    }
  }
`;

export const InputWrapper = styled.div`
  margin-bottom: 25px;
  position: relative;
  .errorMsg {
    font-size: 12px;
    line-height: 0;
    position: absolute;
    bottom: -11px;
    color: red;
  }
`;
