import React from "react";
import { Container, Row, Col } from "react-grid-system";

// Components
import TopHeader from "../../components/header";
import Cover from "../../components/home/cover";
import InputBox from "../../components/form/input";
import TextAreaBox from "../../components/form/textarea";
import Button from "../../components/button/mediumButton";

// Assets
import HomeShowcase from "../../assets/img/cover.jpg";

// Styles
import * as styles from "./index.style";

class Contact extends React.Component {
  constructor() {
    super();
    this.state = {
      fields: {
        username: "",
        emailid: "",
        message: ""
      },
      errors: {}
    };
  }

  handleChange = e => {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  };

  submituserRegistrationForm = e => {
    e.preventDefault();
    this.validateForm(() => {
      if (
        Object.keys(this.state.errors).filter(
          x => this.state.errors[x] !== undefined
        ).length === 0
      ) {
        this.props.updateSuccessState(true);

        const txt = Object.keys(this.state.fields).reduce(
          (acc, x) => `${acc} <br> ${x}: ${this.state.fields[x]}`,
          ""
        );

        console.log(txt);

        // const emailList = [
        //   "dhruv@guesthouser.com",
        //   "guido@guesthouser.com",
        //   "arpit.p@guesthouser.com",
        //   "sahil@guesthouser.com"
        // ];

        // emailList.map(to => {
        //   const pullParams = {
        //     FunctionName: "sendemail",
        //     Payload: JSON.stringify({
        //       from: "support@guesthouser.com",
        //       to: to,
        //       subject: "Support Properly",
        //       html: txt.replace("undefined:", "")
        //     })
        //   };

        //   lambda.invoke(pullParams, function(err, data) {
        //     if (err) {
        //       prompt(err);
        //     } else {
        //       console.log(data);
        //     }
        //   });
        // });
      }
    });
  };

  validateUserName = cb => {
    const {
      fields: { username }
    } = this.state;

    if (username.length !== 0) {
      if (!username.match(/^[a-zA-Z ]*$/)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: "*Please enter alphabet characters only."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            username: "*Please enter your username"
          }
        },
        cb
      );
    }
  };

  validateEmailID = cb => {
    const {
      fields: { emailid }
    } = this.state;

    if (emailid.length !== 0) {
      const pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(emailid)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: "*Please enter valid email-ID."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            emailid: "*Please enter your email-ID"
          }
        },
        cb
      );
    }
  };

  validateMessage = cb => {
    const {
      fields: { message }
    } = this.state;
    if (message.length === 0) {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            message: "*Please enter your message."
          }
        },
        cb
      );
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            message: undefined
          }
        },
        cb
      );
    }
  };

  validateForm = cb => {
    this.validateMessage(() =>
      this.validateEmailID(() => this.validateUserName(cb))
    );
  };
  componentDidMount() {
    window.scroll(0, 0);
  }
  render() {
    console.log(this.props);
    return (
      <>
        <TopHeader />
        <styles.SoloTripContainer>
          <Cover
            fontSize
            coversrc={HomeShowcase}
            subheading=""
            heading="Contact"
          />
          <styles.SoloMain>
            <styles.TripSection>
              <Container fluid style={{ maxWidth: "1140px" }}>
                <Row justify="center">
                  <Col md={8}>
                    <styles.HeadingContent>
                      We would love to hear from you
                    </styles.HeadingContent>
                    <styles.Paragraph>
                      share some thoughts, ideas, advice and suggestions, we
                      would like to hear from you.
                    </styles.Paragraph>
                    <form
                      method="post"
                      name="userRegistrationForm"
                      onSubmit={this.submituserRegistrationForm}
                    >
                      <styles.InputWrapper>
                        <InputBox
                          label="Your name *"
                          name="username"
                          type="text"
                          onChange={this.handleChange}
                        />
                        <span className="errorMsg">
                          {this.state.errors.username}
                        </span>
                      </styles.InputWrapper>
                      <styles.InputWrapper>
                        <InputBox
                          label="You email *"
                          type="email"
                          name="emailid"
                          onChange={this.handleChange}
                        />
                        <span className="errorMsg">
                          {this.state.errors.emailid}
                        </span>
                      </styles.InputWrapper>
                      <styles.InputWrapper>
                        <TextAreaBox
                          label="Your message *"
                          name="message"
                          value={this.state.fields.message}
                          onChange={this.handleChange}
                        />
                        <span className="errorMsg">
                          {this.state.errors.message}
                        </span>
                      </styles.InputWrapper>
                      <Button
                        type="submit"
                        backgroundColor="#00b7b7"
                        textColor="#fff"
                      >
                        Submit
                      </Button>
                    </form>
                  </Col>
                </Row>
              </Container>
            </styles.TripSection>
          </styles.SoloMain>
        </styles.SoloTripContainer>
      </>
    );
  }
}

export default Contact;
