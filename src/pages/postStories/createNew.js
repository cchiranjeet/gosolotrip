import React from "react";
import styled from "styled-components";
import TopHeader from "../../components/header";
// import Cover from "../../components/home/cover";
import DestinationTile from "../../components/blog/destinationTile";
import ResentStories from "../../components/blog/recentStories";
import { Container, Row, Col } from "react-grid-system";
// import InputBox from "../../components/form/input";
import TextAreaBox from "../../components/form/textarea";
// import BlogHeading from "../../components/blogHeading";

// import publishShowcase from "../../assets/img/trip1.jpg";
// import { Link } from "react-router-dom";
// const Form = styled.form``;

const SoloTripContainer = styled.div`
  margin-top: 90px;
`;
const SoloMain = styled.div``;
const TripSection = styled.div`
  position: relative;
  background: linear-gradient(to right, white 50%, #f5f5f5 50%);
  @media (max-width: 768px) {
    margin-bottom: 30px;
  }
`;
const RightContainer = styled.div`
  background: #f5f5f5;
  width: calc(100%);
  background: #f5f5f5;
  padding-left: 35px;
  text-align: right;
  @media (max-width: 768px) {
    padding-left: 15px;
  }
`;
const ColWithPadding = styled(Col)`
  padding-top: 50px;
  @media (max-width: 768px) {
    padding-top: 20px;
  }
`;
const ColWithPaddingMiddle = styled(ColWithPadding)`
  background: white;
  padding-right: 40px !important;
  padding-bottom: 50px;
  @media (max-width: 768px) {
    padding-right: unset;
    padding-bottom: 30px;
  }
`;
class PublishStories extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [{ Paragraph: "" }]
    };
  }
  handleChange(i, e) {
    const { name, value } = e.target;
    let users = [...this.state.users];
    users[i] = { ...users[i], [name]: value };
    this.setState({ users });
  }

  addClick = () => {
    this.setState(prevState => ({
      users: [...prevState.users, { Paragraph: "" }]
    }));
  };

  createUI = () => {
    return this.state.users.map((el, i) => (
      <div key={i}>
        <TextAreaBox
          placeholder="Day 1 Experience"
          name="Paragraph"
          label="Day 1 Experience"
          height="80px"
          value={el.Paragraph || ""}
          onChange={i => this.handleChange(i)}
        />
        <input
          type="button"
          value="remove"
          onClick={i => this.removeClick(i)}
        />
      </div>
    ));
  };
  removeClick = i => {
    let users = [...this.state.users];
    users.splice(i, 1);
    this.setState({ users });
  };

  handleSubmit = event => {
    alert("A name was submitted: " + JSON.stringify(this.state.users));
    event.preventDefault();
  };

  render() {
    return (
      <>
        <TopHeader light blacklogo />
        <SoloTripContainer>
          <SoloMain>
            <TripSection>
              <Container fluid style={{ maxWidth: "1200px" }}>
                <Row>
                  <ColWithPaddingMiddle md={8} xs={12} sm={12}>
                    Comming Soon
                    {/* 
                    <BlogHeading
                      headingtxt="Publish Your Story"
                      paragraph="Adventures may be activities with some potential for physical danger such as traveling, exploring, skydiving, mountain climbing, scuba diving, river rafting or participating in extreme sports"
                    />
                    <Form onSubmit={this.handleSubmit}>
                      <InputBox type="text" name="Name" label="Name" />
                      <InputBox type="email" name="Email" label="Email" />
                      <InputBox
                        type="text"
                        name="Location"
                        label="Where your have been? Ex: Jammu, Srinagar"
                      />

                      <InputBox
                        type="file"
                        name="Banner"
                        label="Blog Banner (size:1920X1254)"
                      />

                      <InputBox
                        type="text"
                        name="Heading"
                        label="Banner Heading"
                      />
                      <InputBox
                        type="text"
                        name="Subheading"
                        label="Blog Slogan"
                      />
                      <InputBox type="text" name="Title" label="Blog Title" />
                      <TextAreaBox
                        label="About the beauty of city where you have been*"
                        name="Paragraph"
                        height="80px"
                      />

                      {this.createUI()}
                      <input
                        type="button"
                        value="add more"
                        onClick={() => this.addClick()}
                      />
                      <input type="submit" value="Submit" />
                    </Form> */}
                  </ColWithPaddingMiddle>
                  <ColWithPadding md={3.5} xs={12} sm={12}>
                    <RightContainer>
                      <DestinationTile />
                      <ResentStories />
                    </RightContainer>
                  </ColWithPadding>
                </Row>
              </Container>
            </TripSection>
          </SoloMain>
        </SoloTripContainer>
      </>
    );
  }
}

export default PublishStories;
