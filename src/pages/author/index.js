/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-script-url */
import React from "react";
import styled from "styled-components";
import TopHeader from "../../components/header";
// import { withRouter } from "react-router-dom";
import { Container, Row, Col, Hidden } from "react-grid-system";
import AddBanner from "../../assets/img/add.jpeg";
import DestinationTile from "../../components/blog/destinationTile";
import ResentStories from "../../components/blog/recentStories";
import BlogHeading from "../../components/blogHeading";
import Blog from "../../components/authorBlog";

import { connect } from "react-redux";

const SoloTripContainer = styled.div`
  margin-top: 90px;
  @media (max-width: 768px) {
    margin-top: 60px;
  }
`;
const SoloMain = styled.div``;
const BlogSection = styled.div`
  background: linear-gradient(to right, white 50%, #f5f5f5 50%);
`;
const RightContainer = styled.div`
  background: #f5f5f5;
  width: calc(100%);
  background: #f5f5f5;
  padding-left: 35px;
  text-align: right;
  @media (max-width: 768px) {
    padding-left: 15px;
  }
`;

const ColWithPadding = styled(Col)`
  padding-top: 50px;
  @media (max-width: 768px) {
    padding-top: 20px;
  }
`;
const Addbanner = styled.div`
  position: sticky;
  top: 100px;
  background: #f5f5f5;
  width: calc(100%);
  background: #f5f5f5;
  padding-left: 35px;
  padding-bottom: 50px;
  text-align: right;
`;
const ColWithPaddingMiddle = styled(ColWithPadding)`
  background: white;
  padding-bottom: 50px;
  padding-right: 50px !important;
`;
class AuthorBlog extends React.Component {
  componentDidMount() {
    window.scroll(0, 0);
  }
  render() {
    return (
      <>
        <TopHeader light blacklogo />
        <SoloTripContainer>
          <SoloMain>
            <BlogSection>
              <Container fluid style={{ maxWidth: "1200px" }}>
                <Row>
                  <ColWithPaddingMiddle md={8.5} xs={12} sm={12}>
                    <BlogHeading
                      headingtxt="BLOG"
                      articleno="9 article"
                      paragraph="Adventures may be activities with some potential for physical danger such as traveling, exploring, skydiving, mountain climbing, scuba diving, river rafting or participating in extreme sports"
                    />
                    <Blog />
                  </ColWithPaddingMiddle>
                  <Hidden xs sm>
                    <ColWithPadding md={3.5} xs={12} sm={12}>
                      <RightContainer>
                        <DestinationTile />
                        <ResentStories />
                      </RightContainer>
                      <Addbanner>
                        <img src={AddBanner} />
                      </Addbanner>
                    </ColWithPadding>
                  </Hidden>
                </Row>
              </Container>
            </BlogSection>
          </SoloMain>
        </SoloTripContainer>
      </>
    );
  }
}

export default AuthorBlog;
