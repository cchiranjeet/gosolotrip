/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-script-url */
import React from "react";
import styled from "styled-components";
import TopHeader from "../../components/header";
import Cover from "../../components/home/cover";
import HomeShowcase from "../../assets/img/trip2.jpg";
import myPhoto from "../../assets/img/photo.jpg";
// import { withRouter } from "react-router-dom";
import { Container, Row, Col, Hidden } from "react-grid-system";

const SoloTripContainer = styled.div``;
const SoloMain = styled.div`
  padding-top: 50px;
  padding-bottom: 50px;
`;
const TrawellEntry = styled.div`
  flex: 1;
  min-width: 0;
  max-width: 600px;
  width: 100%;
  margin-bottom: 50px;
  margin: 0 auto;
  display: block;
`;
const EntryContent = styled.div`
  margin-bottom: 25px;
`;
const TrawellAuthor = styled.div`
  margin-bottom: 50px;
  img {
    float: left;
    margin-right: 20px;
  }
  h5 {
    font-size: 1.5rem;
    margin-top: -12px;
    display: inline-block;
    vertical-align: top;
  }
  .entry-meta {
    color: rgba(74, 74, 74, 1);
    margin-top: -8px;
    margin-bottom: 13px;
    font-size: 0.8rem;
    opacity: 0.5;
    display: block;
    font-family: "Open Sans";
    font-weight: 400;
  }
`;
const ExcerptSmall = styled.div`
  margin-left: 170px;
  p {
    font-size: 0.9rem;
    line-height: 1.5;
    margin-bottom: 17px;
    font-family: "Open Sans";
    font-weight: 400;
  }
  a {
    border: 1px solid rgba(51, 51, 51, 0.1);
    background: transparent;
    color: #098da3;
    padding: 5px 15px;
    font-size: 0.8rem;
    margin-top: 0;
    font-family: "Open Sans";
    font-weight: 400;
  }
`;
class AboutAuthor extends React.Component {
  componentDidMount() {
    window.scroll(0, 0);
  }
  render() {
    return (
      <>
        <TopHeader />
        <SoloTripContainer>
          <Cover coversrc={HomeShowcase} heading="Authors" fontSize />
          <SoloMain>
            <Container fluid style={{ maxWidth: "1200px" }}>
              <Row>
                <Col xs={12}>
                  <TrawellEntry>
                    <article>
                      <EntryContent>
                        <TrawellAuthor>
                          <img alt="" src={myPhoto} height="150" width="150" />
                          <h5 class="author-box-title">Chiranjet Singh</h5>
                          <span class="entry-meta entry-meta-small opacity-50">
                            15 articles
                          </span>
                          <ExcerptSmall>
                            <p>
                              I love climbing mountains and snowbording. Music
                              fanatic. Geek that loves minimal and harmonious
                              designs. I'm learning Japanese.
                            </p>
                            <a href="">View stories</a>
                          </ExcerptSmall>
                        </TrawellAuthor>
                      </EntryContent>
                    </article>
                  </TrawellEntry>
                  <TrawellEntry>
                    <article>
                      <EntryContent>
                        <TrawellAuthor>
                          <img alt="" src={myPhoto} height="150" width="150" />
                          <h5 class="author-box-title">Deepanshu Dhiman</h5>
                          <span class="entry-meta entry-meta-small opacity-50">
                            15 articles
                          </span>
                          <ExcerptSmall>
                            <p>
                              I love climbing mountains and snowbording. Music
                              fanatic. Geek that loves minimal and harmonious
                              designs. I'm learning Japanese.
                            </p>
                            <a href="">View stories</a>
                          </ExcerptSmall>
                        </TrawellAuthor>
                      </EntryContent>
                    </article>
                  </TrawellEntry>
                </Col>
              </Row>
            </Container>
          </SoloMain>
        </SoloTripContainer>
      </>
    );
  }
}

export default AboutAuthor;
