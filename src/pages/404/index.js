import React from "react";
import styled from "styled-components";
import TopHeader from "../../components/header";
import Cover from "../../components/home/cover";
import { Container, Row, Col } from "react-grid-system";
import HomeShowcase from "../../assets/img/cover.jpg";
// import { Link } from "react-router-dom";

const SoloTripContainer = styled.div``;
const SoloMain = styled.div`
  margin-top: 70px;
  @media (max-width: 768px) {
    margin-top: 30px;
  }
`;
const TripSection = styled.div`
  margin-bottom: 70px;
  @media (max-width: 768px) {
    margin-bottom: 30px;
  }
`;
const Paragraph = styled.p`
  margin-top: 0;
  margin-bottom: 1.6rem;
  font-weight: 500;
  text-align: center;
  @media (max-width: 768px) {
    margin-bottom: 1rem;
    font-size: 0.9rem;
    &:first-child {
      margin-top: 1rem;
    }
  }
`;

class Error extends React.Component {
  componentDidMount() {
    window.scroll(0, 0);
  }
  render() {
    console.log(this.props);
    return (
      <>
        <TopHeader />
        <SoloTripContainer>
          <Cover
            fontSize
            coversrc={HomeShowcase}
            subheading="Error:- 404 "
            heading="Page Not Found"
          />
          <SoloMain>
            <TripSection>
              <Container fluid style={{ maxWidth: "1140px" }}>
                <Row>
                  <Col md={12}>
                    <Paragraph>
                      The page that you are looking for does not exist on this
                      website. You may have accidentally mistype the page
                      address, or followed an expired link. Anyway, we will help
                      you get back on track. Why not try to search for the page
                      you were looking for:
                    </Paragraph>
                  </Col>
                </Row>
              </Container>
            </TripSection>
          </SoloMain>
        </SoloTripContainer>
      </>
    );
  }
}

export default Error;
