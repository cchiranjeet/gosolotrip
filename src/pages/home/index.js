import React from "react";
import { Container, Row, Col } from "react-grid-system";
import { Link, withRouter } from "react-router-dom";

// Components
import Cover from "../../components/home/cover2";
import SectionHeading from "../../components/heading";
import Button from "../../components/button/mediumButton";
import TopDestination from "../../components/home/travelItem";
import Map from "../../components/map";
import theme from "../../assets/theme/index";

// Assets
import MyPHoto from "../../assets/img/profile.jpg";
import Hill from "../../assets/img/hills2.jpeg";
import Desert from "../../assets/img/desert.jpeg";
import Beache from "../../assets/img/beach1.jpeg";
import HomeShowcase from "../../assets/img/cover.jpg";

// Styles
import * as Styles from "./index.style";

class Home extends React.Component {
  componentDidMount() {
    window.scroll(0, 0);
    this.props.changeHeaderTheme(false);
  }

  render() {
    console.log(this.props);
    return (
      <>
        <Styles.SoloTripContainer>
          <Cover
            Showbutton
            imgUrl={HomeShowcase}
            heading="Where will you go next?"
            subheading=" Welcome to Gosolotrip, a place for travelers, and adventurers. Pack
          your bags, hit the road and don't forget to write down all of your
          amazing stories!"
          />
          <Styles.SoloMain>
            <Styles.TripSection>
              <Container fluid style={{ maxWidth: "1200px" }}>
                <Row>
                  <Col md={12}>
                    <SectionHeading>ABOUT ME</SectionHeading>
                  </Col>
                </Row>
                <Row>
                  <Col md={6}>
                    <Styles.Img src={MyPHoto} alt="aboutMe" />
                  </Col>
                  <Col md={6}>
                    <Styles.Paragraph>
                      Keeping one’s self going is a difficult thing to do. There
                      are a million distractions that occur every day and that
                      can mean that we do not stay on track with what we should
                      be doing. Self-motivation is something that does not come
                      easy to a lot of people and that means that there are some
                      steps that need to be taken before you can become
                      motivated to the fullest extent.
                    </Styles.Paragraph>
                    <Styles.Paragraph>
                      Of course, there are some other matters that first need to
                      be taken care of. If there are a lot of distractions that
                      keep you from doing what you need to be doing then you
                      need to make some changes. This is really the first step
                      towards becoming self-motivated in any form.
                    </Styles.Paragraph>
                    <Styles.ButtonWrapper>
                      <Link to="/blog">
                        <Button
                          backgroundColor={theme.primaryColor}
                          textColor={theme.whiteColor}
                        >
                          Read my Stories
                        </Button>
                      </Link>
                      <Button
                        backgroundColor="transparent"
                        textColor={theme.primaryColor}
                        buttonborder="1px solid rgba(51,51,51,0.1)"
                        buttonmargin="0 0 0 15px"
                      >
                        Learn More
                      </Button>
                    </Styles.ButtonWrapper>
                  </Col>
                </Row>
              </Container>
            </Styles.TripSection>
          </Styles.SoloMain>

          <Styles.SoloMain>
            <Styles.TripSection>
              <Container fluid style={{ maxWidth: "1200px" }}>
                <Row>
                  <Col md={12}>
                    <SectionHeading>WHERE I'VE BEEN</SectionHeading>
                  </Col>
                </Row>
                <Row>
                  <Col md={12}>
                    <Map
                      zoom={5}
                      initialCenter={{ lat: 22.2183, lng: 80.1828 }}
                      markers={[
                        {
                          name: "Jaisalmer",
                          position: { lat: 26.9157, lng: 70.9083 }
                        },
                        {
                          name: "Udaipur",
                          position: { lat: 24.5854, lng: 73.7125 }
                        },
                        {
                          name: "Jaipur",
                          position: { lat: 26.9124, lng: 75.7873 }
                        },
                        {
                          name: "Agra",
                          position: { lat: 27.1767, lng: 78.0081 }
                        },
                        {
                          name: "Manali",
                          position: { lat: 32.2396, lng: 77.1887 }
                        },
                        {
                          name: "Coimbatore",
                          position: { lat: 11.0168, lng: 76.9558 }
                        },
                        {
                          name: "Goa",
                          position: { lat: 15.2993, lng: 74.124 }
                        },
                        {
                          name: "Varanasi",
                          position: { lat: 25.3176, lng: 82.9739 }
                        },
                        {
                          name: "Mcleodganj",
                          position: { lat: 32.2426, lng: 76.3213 }
                        },
                        {
                          name: "Mcleodganj",
                          position: { lat: 25.5941, lng: 85.1376 }
                        }
                      ]}
                    />
                  </Col>
                </Row>
              </Container>
            </Styles.TripSection>
          </Styles.SoloMain>

          <Styles.SoloMain>
            <Styles.TripSection>
              <Container fluid style={{ maxWidth: "1200px" }}>
                <Row>
                  <Col md={12}>
                    <SectionHeading>TOP DESTINAMTIONS</SectionHeading>
                  </Col>
                </Row>
                <Row>
                  <Col md={4} xs={12} sm={12}>
                    <TopDestination
                      url="/explore/himachal_pradesh"
                      BottomColor="#d32f2f;"
                      Heading="Himachal Pradesh"
                      subheading="2 article"
                      src={Hill}
                    />
                  </Col>
                  <Col md={4} xs={12} sm={12}>
                    <TopDestination
                      url="/explore/rajasthan"
                      BottomColor="#ef6c00;"
                      Heading="Rajasthan"
                      subheading="1 article"
                      src={Desert}
                    />
                  </Col>
                  <Col md={4} xs={12} sm={12}>
                    <TopDestination
                      url="/explore/beaches"
                      BottomColor="#7cb342;"
                      Heading="GOA"
                      subheading="1 article"
                      src={Beache}
                    />
                  </Col>
                </Row>
              </Container>
            </Styles.TripSection>
          </Styles.SoloMain>
        </Styles.SoloTripContainer>
      </>
    );
  }
}

export default withRouter(Home);
