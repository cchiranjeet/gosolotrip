import styled from "styled-components";

export const SoloTripContainer = styled.div``;
export const SoloMain = styled.div`
  margin-top: 50px;
  @media (max-width: 768px) {
    margin-top: 30px;
  }
`;
export const TripSection = styled.div`
  margin-bottom: 50px;
  @media (max-width: 768px) {
    margin-bottom: 30px;
  }
`;
export const Paragraph = styled.p`
  margin-top: 0;
  margin-bottom: 1.6rem;
  font-weight: 500;
  @media (max-width: 768px) {
    margin-bottom: 1rem;
    font-size: 0.9rem;
    &:first-child {
      margin-top: 1rem;
    }
  }
`;
export const Img = styled.img`
  border-radius: 4px;
`;
export const ButtonWrapper = styled.div``;
