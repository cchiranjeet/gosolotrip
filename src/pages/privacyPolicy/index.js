import React from "react";
import styled from "styled-components";
import Topheader from "../../components/header";
import { Container, Row, Col } from "react-grid-system";

const PrivacyPolcyContainer = styled.div`
  margin-top: 90px;
  padding-top: 50px;
  padding-bottom: 25px;
  p {
    margin-bottom: 25px;
  }
  h1 {
    margin-bottom: 30px;
    width: 100%;
    text-align: center;
  }
  h4 {
    color: #00b7b7;
    margin-bottom: 10px;
  }
`;
class Privacy extends React.Component {
  componentDidMount() {
    window.scroll(0, 0);
  }
  render() {
    return (
      <>
        <Topheader light blacklogo />
        <Container fluid style={{ maxWidth: "1200px" }}>
          <Row>
            <Col md={12}>
              <PrivacyPolcyContainer>
                <h1>Privacy Policy</h1>
                <div
                  className="entry-content"
                  itemProp="mainContentOfPage"
                  itemScope
                  itemType="http://schema.org/WebPageElement"
                >
                  <p>
                    GoSoloTrip respects your privacy and is committed to
                    protecting it. The purpose of this Privacy Policy is to
                    inform you what personally identifiable information we may
                    collect and how it may be used. This statement only applies
                    to this Website.
                  </p>
                  <h1>What personal data we collect and why we collect it</h1>
                  <h4>
                    Information that you voluntarily submit to the Website
                  </h4>
                  <p>
                    When you leave a comment or subscribe to our newsletter, the
                    Website generally, collects personal information from you
                    such as your name or email address.
                  </p>
                  <h4>Information We Collect from Others</h4>
                  <p>
                    The Website uses social media commenting systems and it may
                    happen that you choose your social channels to comment or
                    share a post. Besides your comment appearing on the website,
                    the website may also, receive information about you from
                    these channels – such as Facebook, Instagram, Twitter,
                    Google, Linkedin Or more etc.
                  </p>
                  <h4>Automatically-Collected Information</h4>
                  <p>
                    Information about you and your device is automatically
                    collected by the website when you access it. When you browse
                    the website, a log of your IP address, operating system
                    type, browser type, referring website, pages you viewed, and
                    the dates/times of your access and actions taken – such as
                    the links accessed is collected by it.
                  </p>
                  <h4>Cookies</h4>
                  <p>
                    Cookies, which are small data files stored on the browser by
                    the website have information on your session cookies (
                    <em>which expire when you close the browser</em>) and
                    persistent cookies (
                    <em>that stay on your browser until deleted)</em>.&nbsp;The
                    Website logs in this information to provide you with a more
                    personalized experience on the Website.
                  </p>
                  <h3>How Your Information May Be Used</h3>
                  <div className="et-learn-more et-open clearfix">
                    <h4 className="heading-more open">
                      We may use the information collected in the following
                      ways:
                    </h4>
                    <div className="learn-more-content">
                      <ul>
                        <li>
                          For the Maintenance and the Operation of the Website
                        </li>
                        <li>
                          Creation of an account to identify you as a user of
                          this Website
                        </li>
                        <li>Customize the Website for you</li>
                        <li>
                          Share promotional information through a newsletter,
                          with an option to opt-out in each email.
                        </li>
                        <li>
                          Share any possible administrative or confirmation
                          messages.
                        </li>
                        <li>Share updates and alerts</li>
                        <li>
                          Have a conversation and respond to comments and
                          queries that you may have
                        </li>
                        <li>
                          Protect and Deter any unauthorized or illegal
                          activities
                        </li>
                        <li>Track and show advertising on the Website.</li>
                      </ul>
                      <h3>Third-Party Use of Information</h3>
                    </div>
                  </div>
                  <p>
                    The Website may use third-party service providers to service
                    various aspects of the operations. Each third-party service
                    provider’s use of your personal information is dictated by
                    their respective privacy policies.
                  </p>
                  <p>
                    The Website currently uses the following third-party service
                    providers:
                  </p>
                  <h4>Facebook Integration:</h4>
                  <p>
                    Connecting your Facebook with GoSoloTrip will provide us
                    with some information that will be used for analytics. It is
                    up to the user's choice to integrate Facebook with their
                    GoSoloTrip account for convenience, and this service can be
                    canceled at any point in user settings. If given permission,
                    GoSoloTrip will post on your wall on your behalf about your
                    latest activity on the site. Again, this option is optional
                    and can be disabled at any point in the user settings.
                  </p>
                  <h4>Google Analytics</h4>
                  <p>
                    Google Analytics may capture your IP address, but no other
                    personal information is captured by Google Analytics. This
                    is used to track Website Usage and provide information to
                    referring websites as well as track user actions on the
                    Website.
                  </p>
                  <h4>Mail Chimp</h4>
                  <p>
                    The Website sends you emails and newsletters using this
                    service. For this, your name and email address are stored by
                    us. Please refer to Mail Chimp’s privacy policy for further
                    information. This information is not shared with any other
                    third-party applications. Unless required by law, the
                    Website will not sell, distribute, or reveal your email
                    addresses or other personal information without your
                    consent.
                  </p>
                  <h3>Anonymous Data</h3>
                  <p>
                    Data that is collected from analytics or cookies are
                    examples of this subhead. The data does not identify you
                    alone and is generally shared with parties for marketing,
                    advertising, and other similar needs.
                  </p>
                  <h3>Publicly Visible Information</h3>
                  <p>
                    When you leave a comment on the Website, information like
                    your name or website is publically visible. Your email
                    address will never be publically displayed.&nbsp;Users may
                    see your username, avatar, profile description and website
                    information.
                  </p>
                  <h3>Cookies</h3>
                  <p>
                    Cookies on the Website store your preferences, record
                    user-specific information on what pages users access or
                    visit,&nbsp;ensure&nbsp;that visitors are not repeatedly
                    sent the same banner ads. They also, help customize Website
                    content based on the visitor’s browser type or other
                    information that the visitor sends. Cookies may also be used
                    by third-party services, such as Google Analytics, as
                    described herein.
                  </p>
                  <p>
                    Users may, at any time, prevent the setting of cookies, by
                    the Website, by using a corresponding setting of your
                    internet browser and may thus permanently deny the setting
                    of cookies. Furthermore, already set cookies may be deleted
                    at any time via an Internet browser or other software
                    programs. This is possible in all popular Internet browsers.
                    However, if users deactivate the setting of cookies in your
                    Internet browser, not all functions of the Website may be
                    entirely usable.
                  </p>
                  <h3>Advertising</h3>
                  <h4>Display Ads</h4>
                  <p>
                    The Website may use third-party advertising companies to
                    serve content and advertisements when you visit the Website,
                    which may use cookies, as noted above.
                  </p>
                  <h4>Retargeting&nbsp;Ads</h4>
                  <p>
                    From time to time, the Website may engage
                    in&nbsp;remarketing&nbsp;efforts with third-party companies,
                    such as Google, Facebook, or Instagram, in order to market
                    the Website. These companies use cookies to serve ads based
                    on someone’s past visits to the Website.
                  </p>

                  <h4>Newsletters</h4>
                  <p>
                    Our Website Newsletter can be used for the purpose of
                    updating you or for marketing purposes. For this purpose,
                    these newsletters are tagged with a tracking pixel that
                    allows the Website to analyze the success of the emails.
                  </p>
                  <h3>Rights Related to Your Personal Information</h3>
                  <h4>Opt-out</h4>
                  <p>
                    You may opt-out of future email communications by following
                    the unsubscribe links in our emails. You may also notify us
                    at gosolotrip@gmail.com to be removed from our mailing list.
                  </p>
                  <h4>Amend</h4>
                  <p>
                    You may contact us at gosolotrip@gmail.com to amend or
                    update your personal information.
                  </p>
                  <h4>Sensitive Personal Information</h4>
                  <p>
                    Please do not submit sensitive personal information to the
                    Website. This includes your social security number,
                    information regarding race or ethnic origin, political
                    opinions, religious beliefs, health information, criminal
                    background, or trade union memberships. In case you do, it
                    will be subject to this Privacy Policy.
                  </p>
                  <h4>Children’s Information</h4>
                  <p>
                    The Website does not knowingly collect any personally
                    identifiable information from children under the age of 16.
                    If a parent or guardian believes that the Website has
                    personally identifiable information of a child under the age
                    of 16 in its database, please contact the Website
                    immediately at gosolotrip@gmail.com. The Website will do its
                    best to delete this information to the best of its ability.
                  </p>
                  <h3>Contact Information</h3>
                  <p>
                    At any time, please contact us at gosolotrip@gmail.com for
                    questions related to this Privacy Policy.
                  </p>
                  <p>
                    <strong>Last updated: feb 01, 2020.</strong>
                  </p>
                </div>
              </PrivacyPolcyContainer>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}
export default Privacy;
