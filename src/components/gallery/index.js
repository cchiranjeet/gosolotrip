/* eslint-disable jsx-a11y/img-redundant-alt */
import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import Swiper from "react-id-swiper";
import CrossIMG from "../../assets/img/cross1.png";

const Gallery = styled.div`
  margin-bottom: 30px;

  ul {
    display: flex;
    flex-wrap: wrap;
    list-style-type: none;
    margin: 0;
    li {
      width: calc((100% - 1px * 2) / 3);
      margin-right: 1px;
      margin: 0 0px 0px;
      padding: 0;
      display: flex;
      flex-grow: 1;
      flex-direction: column;
      justify-content: center;
      position: relative;
      list-style: inside;
      text-align: center;
      vertical-align: top;
      cursor: pointer;
      &:hover {
        span {
          img {
            -webkit-transform: scale(1.1);
            -moz-transform: scale(1.1);
            -o-transform: scale(1.1);
            -ms-transform: scale(1.1);
          }
        }
      }
      figure {
        display: flex;
        align-items: flex-end;
        justify-content: flex-start;
        margin: 0.5px;
        height: 100%;
      }
      span {
        height: 100%;
        flex: 1;
        -o-object-fit: cover;
        object-fit: cover;
        overflow: hidden;
        box-shadow: none;
        width: 100%;
        color: rgba(9, 141, 163, 0.7);
        transition: all 0.15s ease-in-out;
        background: 0 0;
        display: inline-block;
        max-width: 100%;
        &:hover {
          background: rgba(9, 141, 163, 0.1);
          text-decoration: none;
          outline: 0;
        }
        img {
          height: 100%;
          flex: 1;
          -o-object-fit: cover;
          object-fit: cover;
          width: 100%;
          display: block;
          transition: transform 0.45s, -webkit-transform 0.45s;
          will-change: transform;
          vertical-align: middle;
          border-style: none;
        }
      }
    }
  }
`;
const Modal = styled.div`
  position: fixed; /* Stay in place */
  z-index: 999999; /* Sit on top */
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 100%;
  height: calc(100% + 1px);
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0, 0, 0); /* Fallback color */
  background-color: rgba(0, 0, 0, 0.89);
  display: flex;
  justify-content: center;
  align-items: center;
`;
const ModalContent = styled.div`
  margin: auto;
  width: 100%;
  height: 100vh;
  position: relative;

  .swiper-container {
    padding: 40px 0;
    height: 100vh;
    .swiper-button-next,
    .swiper-button-prev {
      color: #fff;
    }
    .swiper-pagination-fraction {
      top: 10px;
      font-weight: 700;
      color: #fff;
      bottom: unset;
      left: 15px;
      width: auto;
    }
  }
  .slide {
    width: 100%;
    height: 100%;
    background-size: contain;
    background-position: center center;
    background-repeat: no-repeat;
  }
`;
const Close = styled.span`
  color: #fff;
  display: block;
  position: absolute;
  width: 30px;
  top: -35px;
  right: 8px;
  font-size: 60px;
  z-index: 99999;
  cursor: pointer;
  img {
  }
`;

class TopDestination extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      Slideindex: 0
    };
  }

  onOpenModal = index => {
    console.log("slide", index);
    this.setState({ showModal: true, Slideindex: index });
    document.body.style.overflow = "hidden";
  };

  onCloseModal = () => {
    this.setState({ showModal: false });
    document.body.style.overflow = "unset";
  };

  render() {
    const params = {
      // Add modules you need
      slidesPerView: 1,
      freeMode: true,
      loop: true,
      initialSlide: this.state.Slideindex,
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      },
      pagination: {
        el: ".swiper-pagination",
        type: "fraction"
      }
    };
    return (
      <>
        {Object.keys(this.props.galleryImg).length > 0 && (
          <Gallery>
            <ul>
              {this.props.galleryImg.map((item, index) => {
                return (
                  <li
                    key={index}
                    className="blocks-gallery-item"
                    onClick={() => this.onOpenModal(index)}
                  >
                    <figure>
                      <span>
                        <img src={item} alt="jaipur Images" />
                      </span>
                    </figure>
                  </li>
                );
              })}
            </ul>
          </Gallery>
        )}
        {this.state.showModal && (
          <Modal>
            <ModalContent modal={this.state.modal}>
              <Close onClick={this.onCloseModal}>
                <img src={CrossIMG} alt="image gallery" />
              </Close>
              <Swiper {...params}>
                {this.props.galleryImg.map((item, index) => {
                  return (
                    <div
                      key={index}
                      className="slide"
                      style={{ backgroundImage: `url(${item})` }}
                    >
                      {/* <img src={item} alt="" /> */}
                    </div>
                  );
                })}
              </Swiper>
            </ModalContent>
          </Modal>
        )}
      </>
    );
  }
}

export default TopDestination;
