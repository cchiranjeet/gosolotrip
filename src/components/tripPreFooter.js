/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import styled from "styled-components";
import Swiper from "react-id-swiper";
import Button from "../components/button/mediumButton";
import { Link } from "react-router-dom";

const TripPreFooterContainer = styled.div`
  background: #098da3;
  color: #ffffff;
  text-align: center;
  position: relative;
  overflow: hidden;
  margin-bottom: -1px;
`;

const WidgetInstagram = styled.div`
  border-top: 10px solid #098da3;
  position: relative;
  /* transition: all 0.5s ease; */
  .swiper-button-next,
  .swiper-button-prev {
    width: 50px;
    height: 50px;
    transition: all 0.5s ease-in-out;
    opacity: 0;
    background: #fff;
    border-radius: 100px;
    color: #333;
    &:after {
      font-size: 16px;
      font-weight: 700;
    }
    &:hover {
      background: #098da3;
      color: #fff;
    }
  }
  &:hover {
    .swiper-button-next,
    .swiper-button-prev {
      background: #fff;
      border-radius: 100px;
      display: flex;
      opacity: 1;
      width: 50px;
      height: 50px;
      color: #333;
      &:after {
        font-size: 16px;
        font-weight: 700;
      }
      &:hover {
        background: #098da3;
        color: #fff;
      }
      @media (max-width: 768px) {
        display: none;
      }
    }
    > a:first-child {
      opacity: 1;
    }
  }
  .swiper-container {
    .swiper-slide {
      cursor: pointer;
      max-height: 230px;
      overflow: hidden;
      img {
        transition: transform 0.45s ease-in-out;
      }
      &:hover {
        img {
          -webkit-transform: scale(1.1);
          -moz-transform: scale(1.1);
          -o-transform: scale(1.1);
          -ms-transform: scale(1.1);
        }
      }
    }
  }
`;

const InstaFollowBtn = styled.a`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 9;
  /* transition: all 0.5s ease-in-out; */
  opacity: 0;

  button {
    font-weight: 500;
  }
  @media (max-width: 768px) {
    display: none;
  }
`;

class TripPreFooter extends React.Component {
  render() {
    const params = {
      // Add modules you need
      slidesPerView: 6,
      freeMode: true,
      breakpoints: {
        1024: {
          slidesPerView: 5.5
        },
        768: {
          slidesPerView: 4.5
        },
        640: {
          slidesPerView: 3.2
        },
        320: {
          slidesPerView: 2.2
        }
      },
      loop: true,
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      }
    };
    return (
      <TripPreFooterContainer>
        <WidgetInstagram>
          <InstaFollowBtn
            href="https://www.instagram.com/gosolotrip.in"
            target="_blank"
          >
            <Button backgroundColor="#fff" textColor="#333">
              FOLLOW
            </Button>
          </InstaFollowBtn>
          <Swiper {...params}>
            <a href="">
              <img
                alt=""
                src="https://scontent-ort2-1.cdninstagram.com/v/t51.2885-15/e35/c0.180.1440.1440a/s240x240/80842298_205784753914248_8052116747922648157_n.jpg?_nc_ht=scontent-ort2-1.cdninstagram.com&_nc_cat=100&_nc_ohc=DlOkjtKF7MsAX8LLtd5&oh=5a5a67ce71bb4a1ee432ac43f35ba712&oe=5EAADB2D"
              />
            </a>
            <a href="">
              <img
                alt=""
                src="https://scontent-ort2-1.cdninstagram.com/v/t51.2885-15/e35/c0.156.1249.1249a/s240x240/82060627_774991459680245_1946365715737429365_n.jpg?_nc_ht=scontent-ort2-1.cdninstagram.com&_nc_cat=105&_nc_ohc=YsPQkOx21eMAX_5yLxe&oh=a4c3ddcab6f55e9905634d2fe370bbc1&oe=5E9D2572"
              />
            </a>
            <a href="">
              <img
                alt=""
                src="https://scontent-ort2-1.cdninstagram.com/v/t51.2885-15/e35/c0.156.1249.1249a/s240x240/79960240_2574110679513199_5269001481457227126_n.jpg?_nc_ht=scontent-ort2-1.cdninstagram.com&_nc_cat=107&_nc_ohc=bTfdKBKnmsgAX-xVkPW&oh=4590fcdece082fa5388f02f6e5f10787&oe=5E98AA6E"
              />
            </a>
            <a href="">
              <img
                alt=""
                src="https://scontent-ort2-1.cdninstagram.com/v/t51.2885-15/e35/c0.180.1440.1440a/s240x240/81615774_472874433422821_8013361135650115691_n.jpg?_nc_ht=scontent-ort2-1.cdninstagram.com&_nc_cat=101&_nc_ohc=AHFcFH0XDJ0AX9un53Q&oh=874e02c7f005e47297599138856ea4fd&oe=5E9ED1C5"
              />
            </a>
            <a href="">
              <img
                alt=""
                src="https://scontent-ort2-1.cdninstagram.com/v/t51.2885-15/e35/c0.156.1249.1249a/s240x240/82763506_1013924082324004_107643454021289748_n.jpg?_nc_ht=scontent-ort2-1.cdninstagram.com&_nc_cat=104&_nc_ohc=yHKqbJC5Hh4AX-Xf_C6&oh=98164830958fd9099f6f367d2d24f045&oe=5ED46B52"
              />
            </a>
            <a href="">
              <img
                alt=""
                src="https://scontent-ort2-1.cdninstagram.com/v/t51.2885-15/e35/c0.180.1440.1440a/s240x240/80842298_205784753914248_8052116747922648157_n.jpg?_nc_ht=scontent-ort2-1.cdninstagram.com&_nc_cat=100&_nc_ohc=DlOkjtKF7MsAX8LLtd5&oh=5a5a67ce71bb4a1ee432ac43f35ba712&oe=5EAADB2D"
              />
            </a>
            <a href="">
              <img
                alt=""
                src="https://scontent-ort2-1.cdninstagram.com/v/t51.2885-15/e35/c0.156.1249.1249a/s240x240/82060627_774991459680245_1946365715737429365_n.jpg?_nc_ht=scontent-ort2-1.cdninstagram.com&_nc_cat=105&_nc_ohc=YsPQkOx21eMAX_5yLxe&oh=a4c3ddcab6f55e9905634d2fe370bbc1&oe=5E9D2572"
              />
            </a>
            <a href="">
              <img
                alt=""
                src="https://scontent-ort2-1.cdninstagram.com/v/t51.2885-15/e35/c0.156.1249.1249a/s240x240/79960240_2574110679513199_5269001481457227126_n.jpg?_nc_ht=scontent-ort2-1.cdninstagram.com&_nc_cat=107&_nc_ohc=bTfdKBKnmsgAX-xVkPW&oh=4590fcdece082fa5388f02f6e5f10787&oe=5E98AA6E"
              />
            </a>
            <a href="">
              <img
                alt=""
                src="https://scontent-ort2-1.cdninstagram.com/v/t51.2885-15/e35/c0.180.1440.1440a/s240x240/81615774_472874433422821_8013361135650115691_n.jpg?_nc_ht=scontent-ort2-1.cdninstagram.com&_nc_cat=101&_nc_ohc=AHFcFH0XDJ0AX9un53Q&oh=874e02c7f005e47297599138856ea4fd&oe=5E9ED1C5"
              />
            </a>
            <a href="">
              <img
                alt=""
                src="https://scontent-ort2-1.cdninstagram.com/v/t51.2885-15/e35/c0.156.1249.1249a/s240x240/82763506_1013924082324004_107643454021289748_n.jpg?_nc_ht=scontent-ort2-1.cdninstagram.com&_nc_cat=104&_nc_ohc=yHKqbJC5Hh4AX-Xf_C6&oh=98164830958fd9099f6f367d2d24f045&oe=5ED46B52"
              />
            </a>
          </Swiper>
        </WidgetInstagram>
      </TripPreFooterContainer>
    );
  }
}
export default TripPreFooter;
