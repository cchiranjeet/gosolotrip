import React from "react";
import styled from "styled-components";
import Button from "../button/mediumButton";
import { Link } from "react-router-dom";

const CommentForm = styled.form`
  margin-bottom: 20px;
  p {
    margin-bottom: 15px;
    label {
      font-size: 0.8rem;
      margin-bottom: 5px;
      line-height: 1.3;
      display: block;
      font-family: "Open Sans";
      font-weight: 400;
    }
    textarea {
      overflow: auto;
      resize: vertical;
      border: 1px solid rgba(0, 0, 0, 0.1);
      outline: 0;
      line-height: 1;
      height: 150px;
      display: block;
      width: 100%;
      margin: 0;
      border-radius: 3px;
      padding: 10px;
      font-size: 0.9rem;
    }
    input[type="text"],
    input[type="email"],
    input[type="url"] {
      padding: 5px 10px;
      font-size: 0.9rem;
      border: 1px solid rgba(0, 0, 0, 0.1);
      outline: 0;
      line-height: 1;
      width: 100%;
      margin: 0;
      height: 40px;
      vertical-align: baseline;
      border-radius: 3px;
      background: 0 0;
      overflow: visible;
    }
  }
`;
const CommentNotes = styled.p`
  font-size: 0.8rem;
  margin-bottom: 25px;
  font-family: "Open Sans";
  font-weight: 400;
`;
const EmailNote = styled.span``;

const CommentFormCookiesConsent = styled.p`
  display: flex;

  input[type="checkbox"] {
    position: relative;
    top: 2px;
    box-sizing: border-box;
    padding: 0;
    overflow: visible;
  }
  label {
    display: inline-block;
    margin-left: 8px;
    font-size: 0.8rem;
    margin-bottom: 5px;
  }
`;
const FormSubmit = styled.p`
  margin-top: 10px;
`;
const CommentPostForm = props => {
  return (
    <CommentForm>
      <CommentNotes>
        <EmailNote>Your email address will not be published.</EmailNote>{" "}
        Required fields are marked
        <span className="required">*</span>
      </CommentNotes>
      <p>
        <label htmlFor="comment">Comment</label>{" "}
        <textarea
          id="comment"
          name="comment"
          cols={45}
          rows={8}
          maxLength={65525}
          required="required"
          defaultValue={""}
        />
      </p>
      <p>
        <label htmlFor="author">
          Name <span className="required">*</span>
        </label>{" "}
        <input
          id="author"
          name="author"
          type="text"
          size={30}
          maxLength={245}
          required="required"
        />
      </p>
      <p>
        <label htmlFor="email">
          Email <span className="required">*</span>
        </label>{" "}
        <input
          id="email"
          name="email"
          type="email"
          size={30}
          maxLength={100}
          required="required"
        />
      </p>
      <p>
        <label htmlFor="url">Website</label>{" "}
        <input id="url" name="url" type="url" size={30} maxLength={200} />
      </p>
      <CommentFormCookiesConsent>
        <input
          id="wp-comment-cookies-consent"
          name="wp-comment-cookies-consent"
          type="checkbox"
          defaultValue="yes"
        />
        <label htmlFor="wp-comment-cookies-consent">
          Save my name, email, and website in this browser for the next time I
          comment.
        </label>
      </CommentFormCookiesConsent>

      <FormSubmit>
        <Button backgroundColor="#098DA3" textColor="#fff">
          POST COMMENT
        </Button>
      </FormSubmit>
    </CommentForm>
  );
};

export default CommentPostForm;
