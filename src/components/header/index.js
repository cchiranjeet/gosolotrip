/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-script-url */
import React from "react";
import * as firebase from "firebase";
import "firebase/auth";
import { slide as Menu } from "react-burger-menu";
import { Container, Row, Col, Hidden, Visible } from "react-grid-system";
import { Link } from "react-router-dom";
import Logo from "../../assets/img/logo.svg";
import BlackLogo from "../../assets/img/black_logo.svg";

import { withRouter } from "react-router-dom";
import Modal from "../../components/modal";
import FormSignup from "../form/SignupForm";
import FormLogin from "../form/loginForm";
import popupImage from "../../assets/img/hills2.jpeg";
import { isAppLoaded, Initialize } from "../../index";

//styles
import * as styles from "./index.style";

const getUser = () =>
  JSON.parse((x => (x ? x : null))(localStorage.getItem("user")));

const iosScrollListener = callBack => {
  const element = document.createElement("div");
  element.id = "hidden";
  element.style = "position: absolute; opacity: 0; top:-30px";
  document.body.append(element);

  const fn = () => {
    element.innerHTML = window.scrollY;
    callBack();
  };
  window.addEventListener("scroll", fn);

  return () => window.removeEventListener("scroll", fn);
};

class TopHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      SignForm: false,
      isModal: false,
      headerClass: "",
      menuOpen: false,
      showSubmenu: null,
      user: getUser()
    };
  }
  handleStateChange = ({ isOpen }) => {
    this.setState({ menuOpen: isOpen }, () => {
      if (this.state.menuOpen === true) {
        document.body.style.overflow = "hidden";
      } else {
        document.body.style.overflow = "unset";
      }
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.setState({ menuOpen: false });
    }
  }

  componentDidMount() {
    if (!isAppLoaded()) {
      Initialize();
    }

    this.remove = iosScrollListener(() => {
      const offset = 100;
      if (window.scrollY > offset) {
        this.setState(() => ({ headerClass: "active" }));
      } else {
        this.setState(() => ({ headerClass: "" }));
      }
    });
    document.body.style.overflow = "unset";
  }

  doSomething = ID => {
    this.props.history.push({
      pathname: `/explore/` + ID
    });
  };

  toggleShow = e => {
    this.setState({ showSubmenu: e });
  };

  hide = e => {
    if (e && e.relatedTarget) {
      e.relatedTarget.click();
    }
    this.setState({ show: false });
    // document.body.style.overflow = "unset";
  };

  openModal = () => {
    this.setState({
      isModal: true
    });
  };

  closeModal = () => {
    this.setState({
      isModal: false
    });
  };

  openLoginForm = () => {
    this.setState({
      SignForm: false
    });
  };

  openSignupForm = () => {
    this.setState({
      SignForm: true
    });
  };

  signout = () => {
    firebase
      .auth()
      .signOut()
      .then(() => {
        this.props.setLoggedIn(false);
        localStorage.setItem("user", null);
        this.setState({ user: null });
      })
      .catch(err => {
        console.error(err);
      });
  };

  render() {
    return (
      <>
        <Hidden xs sm>
          <styles.Header
            light={this.props.light}
            className={this.state.headerClass}
          >
            <Container fluid style={{ maxWidth: "1200px" }}>
              <Row>
                <Col md={12}>
                  <styles.Nav>
                    <styles.Slot>
                      <styles.MenuFirst>
                        <styles.MenuList>
                          <styles.List light={this.props.light}>
                            <Link to="">Home</Link>
                          </styles.List>

                          <styles.List tab-index="0" light={this.props.light}>
                            <Link to="#" className="stories">
                              Inspirations
                            </Link>
                            <styles.DropDownList>
                              <styles.ListLink>
                                <Link to="/explore/hills">Mountains</Link>
                              </styles.ListLink>
                              <styles.ListLink>
                                <Link to="/explore/beaches">Beaches</Link>
                              </styles.ListLink>
                              <styles.ListLink>
                                <Link to="/explore/heritage">Heritage</Link>
                              </styles.ListLink>
                              <styles.ListLink>
                                <Link to="/explore/weekend_getways">
                                  Weekend Getaways
                                </Link>
                              </styles.ListLink>
                              {/*
                              <ListLink>
                                <Link to="/explore/road-trip">Road Trip</Link>
                              </ListLink> */}
                            </styles.DropDownList>
                          </styles.List>
                          <styles.List light={this.props.light}>
                            <Link to="#" className="rent">
                              Destinations
                            </Link>
                            <styles.DropDownList>
                              <styles.ListLink>
                                <Link to="/place/himachal_pradesh">
                                  himachal pradesh
                                </Link>
                              </styles.ListLink>
                              <styles.ListLink>
                                <Link to="/place/rajasthan">Rajasthan</Link>
                              </styles.ListLink>
                              <styles.ListLink>
                                <Link to="/place/uttarakhand">uttarakhand</Link>
                              </styles.ListLink>

                              {/* <ListLink>
                                <Link to="/place/southIndia">South India</Link>
                              </ListLink> */}
                              {/* <ListLink>
                                <Link to="/explore/goa">GOA</Link>
                              </ListLink> */}
                            </styles.DropDownList>
                          </styles.List>
                        </styles.MenuList>
                      </styles.MenuFirst>
                    </styles.Slot>
                    <styles.MiddleSlot>
                      <Link to="/">
                        <styles.LogoWrap>
                          {this.props.light || this.state.headerClass ? (
                            <img src={BlackLogo} alt="GoSoloTrip" />
                          ) : (
                            <img src={Logo} alt="GoSoloTrip" />
                          )}
                        </styles.LogoWrap>
                      </Link>
                    </styles.MiddleSlot>
                    <styles.Slot>
                      <styles.MenuSecond>
                        <styles.MenuList light={this.props.light}>
                          <styles.List light={this.props.light}>
                            <Link to="#" className="rent">
                              Publish Storie
                            </Link>
                            <styles.DropDownList>
                              <styles.ListLink>
                                <Link to="/publish-stories">Create New</Link>
                              </styles.ListLink>
                              <styles.ListLink>
                                <Link to="beaches">Import Stories</Link>
                              </styles.ListLink>
                            </styles.DropDownList>
                          </styles.List>
                          <styles.List light={this.props.light}>
                            <Link to="#" className="rent">
                              Rent Now
                            </Link>
                            <styles.DropDownList>
                              <styles.ListLink>
                                <Link to="listbike">Renting a bike</Link>
                              </styles.ListLink>
                              <styles.ListLink>
                                <Link to="beaches">Listing a Bike</Link>
                              </styles.ListLink>
                            </styles.DropDownList>
                          </styles.List>
                          <styles.List light={this.props.light}>
                            {!this.props.loggedIn ? (
                              <Link to="#" onClick={this.openModal}>
                                Login/Signup
                              </Link>
                            ) : (
                              <>
                                <Link to="#" className="rent">
                                  My Account
                                </Link>
                                <styles.DropDownList>
                                  <styles.ListLink>
                                    <Link to="#" onClick={this.signout}>
                                      Logout
                                    </Link>
                                  </styles.ListLink>
                                </styles.DropDownList>
                              </>
                            )}
                            {/* <DropDownList>
                              <ListLink>
                                <Link to="listbike"></Link>
                              </ListLink>
                              <ListLink>
                                <Link to="beaches">Bike on rent</Link>
                              </ListLink>
                            </DropDownList> */}
                          </styles.List>
                        </styles.MenuList>
                      </styles.MenuSecond>
                    </styles.Slot>
                  </styles.Nav>
                </Col>
              </Row>
            </Container>
          </styles.Header>
        </Hidden>
        <Visible xs sm>
          <styles.MobileHeader
            light={this.props.light}
            className={this.state.headerClass}
          >
            <Container fluid>
              <Row>
                <Col xs={4}>
                  <Menu
                    isOpen={this.state.menuOpen}
                    onStateChange={state => this.handleStateChange(state)}
                  >
                    <div>
                      <Link to="/">Home</Link>
                    </div>
                    <div>
                      <span
                        onClick={() => this.toggleShow("Inspirations")}
                        onBlur={this.hide}
                      >
                        Inspirations
                      </span>
                      {this.state.showSubmenu === "Inspirations" && (
                        <ul className="dropdown-menu">
                          <li>
                            <div onClick={() => this.doSomething("hills")}>
                              - Mountains
                            </div>
                          </li>
                          <li>
                            <div onClick={() => this.doSomething("beaches")}>
                              - Beaches
                            </div>
                          </li>
                          <li>
                            <div onClick={() => this.doSomething("heritage")}>
                              - Heritage
                            </div>
                          </li>{" "}
                          <li>
                            <div
                              onClick={() =>
                                this.doSomething("weekend_getways")
                              }
                            >
                              - Weekend Getaways
                            </div>
                          </li>{" "}
                        </ul>
                      )}
                    </div>
                    <div>
                      <span
                        onClick={() => this.toggleShow("Destinations")}
                        onBlur={this.hide}
                      >
                        Destinations
                      </span>
                      {this.state.showSubmenu === "Destinations" && (
                        <ul className="dropdown-menu">
                          <li>
                            <Link to="/place/himachal_pradesh">
                              - Himachal Pradesh
                            </Link>
                          </li>
                          <li>
                            <Link to="/place/rajasthan">- Rajasthan</Link>
                          </li>
                          <li>
                            <Link to="/place/uttarakhand">- Uttarakhand</Link>
                          </li>{" "}
                        </ul>
                      )}
                    </div>
                    <div>
                      <Link to="/publish-stories">Stories</Link>
                    </div>
                    <div>
                      <Link to="/">BUY/RENT</Link>
                    </div>
                    <div>
                      <Link to="/">Account</Link>
                    </div>
                  </Menu>
                </Col>
                <Col xs={4}>
                  <Link to="/" className="logoLink">
                    <div className="mobileLogo">
                      {this.props.light || this.state.headerClass ? (
                        <img src={BlackLogo} alt="gosolotrip logo" />
                      ) : (
                        <img src={Logo} alt="gosolotrip logo" />
                      )}
                    </div>
                  </Link>
                </Col>
                <Col xs={4}></Col>
              </Row>
            </Container>
          </styles.MobileHeader>
        </Visible>
        {this.state.isModal && (
          <Modal
            openModal={this.openModal}
            closeModal={this.closeModal}
            modalwidth="700px"
          >
            <Row>
              <Hidden xs sm>
                <Col md={6}>
                  <img src={popupImage} alt="popimage" />
                </Col>
              </Hidden>
              <Col xs={12} md={6}>
                {this.state.SignForm ? (
                  <styles.FormWraper>
                    <styles.Heading> SIGNUP</styles.Heading>
                    <styles.Subheading>
                      Lorem ipsum get sum eimm
                    </styles.Subheading>
                    <FormSignup
                      loggedIn={this.props.loggedIn}
                      setLoggedIn={this.props.setLoggedIn}
                      closeModal={this.closeModal}
                    />
                    <styles.Text>
                      Already have an account?{" "}
                      <span onClick={this.openLoginForm}>Log in!</span>
                    </styles.Text>
                  </styles.FormWraper>
                ) : (
                  <styles.FormWraper>
                    <styles.Heading>LOGIN</styles.Heading>
                    <styles.Subheading>
                      Lorem ipsum get sum eimm
                    </styles.Subheading>
                    <FormLogin
                      loggedIn={this.props.loggedIn}
                      setLoggedIn={this.props.setLoggedIn}
                      closeModal={this.closeModal}
                    />
                    <styles.Text>
                      Don't have an account?{" "}
                      <span onClick={this.openSignupForm}>Signup!</span>
                    </styles.Text>
                  </styles.FormWraper>
                )}
              </Col>
            </Row>
          </Modal>
        )}
      </>
    );
  }
}

export default withRouter(TopHeader);
