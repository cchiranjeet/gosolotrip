import styled, { keyframes } from "styled-components";
import Arrow from "../../assets/img/arrow.png";
import GreyArrow from "../../assets/img/breadcrum-arrow.png";
import GreenArrow from "../../assets/img/arrow-green.png";

export const stickySlideDown = keyframes`
    0% {
        opacity: 0.7;
        transform: translateY(-100%);
    }
    100% {
        opacity: 1;
        transform: translateY(0);
    }
  `;

export const Header = styled.header`
  position: absolute;
  height: 90px;
  z-index: 99;
  width: 100%;
  top: 0;
  background-color: ${p => (p.light ? "white" : "transparent")};
  box-shadow: ${p => (p.light ? "0 5px 10px 0 rgba(0, 0, 0, 0.1)" : "none")};

  animation-duration: 0.2s;
  transition: 0.2s;
  &.active {
    position: fixed;
    z-index: 100;
    height: 80px;
    transition: none;
    animation-name: ${stickySlideDown};
    top: 0;
    background: #fff;
    box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.1);
    /* box-shadow: 0 7px 19px 0 rgba(0, 0, 0, 0.13); */
    h1 {
      width: 110px;
      padding-top: 5;
    }
    nav {
      height: 80px;
    }
    ul {
      li {
        a {
          color: #333;
          &:hover {
            color: #08b7ce;
          }
        }
      }
    }
    .stories,
    .rent {
      &::after {
        background-image: url(${GreyArrow});
      }
    }
  }
`;
export const Nav = styled.nav`
  flex-wrap: nowrap;
  display: flex;
  align-items: center;
  width: 100%;
  height: 90px;
`;
export const Slot = styled.div`
  flex: 1;
`;
export const MiddleSlot = styled.div`
  flex-grow: 0;
  margin: 0 50px;
  width: 120px;
  a {
    display: block;
  }
`;
export const MenuFirst = styled.div`
  text-align: right;
`;
export const MenuSecond = styled.div`
  text-align: left;
`;
export const MenuList = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
  font-size: 0.8rem;
`;
export const List = styled.li`
  position: relative;
  display: inline-block;
  vertical-align: top;
  margin: 0 20px;
  padding: 0;
  a {
    padding: 15px 0;
    text-decoration: none;
    display: block;
    text-transform: uppercase;
    font-family: "Quicksand";
    font-weight: 700;
    transition: all 0.15s ease-in-out;
    color: ${p => (p.light ? "#333" : "#fff")};
    &:hover {
      color: #08b7ce;
      &.stories,
      &.rent {
        &:after {
          content: "";
          width: 20px;
          height: 20px;
          position: absolute;
          background-image: url(${GreenArrow});
          background-position: 0;
          right: -22px;
          top: 19px;
          transform: rotate(90deg) scale(0.5);
          background-repeat: no-repeat;
        }
      }
    }
    &.stories,
    &.rent {
      &:after {
        content: "";
        width: 20px;
        height: 20px;
        position: absolute;
        background-image: ${p =>
          p.light ? `url(${GreyArrow})` : `url(${Arrow})`};
        background-position: 0;
        right: -22px;
        top: 19px;
        transform: rotate(90deg) scale(0.5);
        background-repeat: no-repeat;
      }
    }
  }
  &:focus ul,
  &:focus-within,
  &:hover ul {
    visibility: visible;
    opacity: 1;
    z-index: 1;
    transform: translateY(0%);
    transition-delay: 0s, 0s, 0.3s;
  }
`;

export const DropDownList = styled.ul`
  visibility: hidden; /* hides sub-menu */
  opacity: 0;
  position: absolute;
  top: 60px;
  right: 0;
  width: 220px;
  border-radius: 4px;
  transform: translateY(-2rem);
  z-index: -1;
  transition: all 0.3s ease-in-out 0s, visibility 0s linear 0.3s,
    z-index 0s linear 0.01s;
  box-shadow: 0 5px 15px 0 rgba(0, 0, 0, 0.25);
  background: #fff;
  padding: 20px;

  &:after {
    margin: 0 0 0 7px;
    content: "";
    top: -12px;
    right: 20px;
    transform: rotate(90deg);
    display: block;
    width: 0;
    height: 0;
    position: absolute;
    border-top: 8px solid transparent;
    border-bottom: 8px solid transparent;
    border-right: 8px solid #fff;
  }
`;
export const LogoWrap = styled.h1`
  padding-top: 10px;
`;
export const ListLink = styled.li`
  padding: 10px;
  &:first-child {
    padding-top: 0;
  }
  &:last-child {
    padding-bottom: 0px;
  }
  a {
    background: #fff;
    text-align: left;
    font-size: 0.8rem;
    padding: 0px 0;
    color: #333;
    font-weight: 700;
  }
`;
export const MobileHeader = styled.div`
  height: 60px;
  background: ${p => (p.light ? "white" : "transparent")};
  width: 100%;
  position: absolute;
  z-index: 999;
  top: 0;
  animation-duration: 0.2s;
  transition: 0.2s;
  .logoLink {
    display: block;
  }
  &.active {
    position: fixed;
    background: #fff;
    height: 50px;
    width: 100%;
    z-index: 999;
    animation-name: ${stickySlideDown};
    top: 0;
    box-shadow: 0 7px 19px 0 rgba(0, 0, 0, 0.13);
    .bm-burger-bars {
      background: #333;
    }
    .mobileLogo {
      width: 80px;
      height: 50px;
      padding-bottom: 8px;
    }
    .bm-burger-button {
      top: 16px;
    }
  }
  .mobileLogo {
    width: 94px;
    display: flex;
    justify-content: center;
    height: 60px;
    margin-top: 3px;
  }
  .bm-burger-button {
    position: relative;
    float: left;
    width: 26px;
    height: 17px;
    top: 18px;
  }

  /* Color/shape of burger icon bars */
  .bm-burger-bars {
    background: ${p => (p.light ? "#333" : "#fff")};
    border-radius: 3px;
  }
  .bm-burger-bars-hover {
    background: #a90000;
  }
  .bm-cross-button {
    height: 24px;
    width: 24px;
  }
  .bm-cross {
    background: #bdc3c7;
  }
  .bm-menu-wrap {
    position: fixed;
    height: 100%;
    margin-left: -15px;
  }
  .bm-menu {
    background: #f5f5f5;
    padding: 2.5em 1.5em 0;
    font-size: 1.15em;
  }
  .bm-morph-shape {
    fill: #373a47;
  }
  .bm-item-list {
    color: #b8b7ad;
    padding: 20px;
    height: auto !important;
    background: #fff;
  }
  .bm-item {
    display: inline-block;
    outline: none;
    a {
      color: #333;
      font-weight: 700;
      font-size: 0.9rem;
      border: none;
      padding: 8px 0;
    }
    span {
      color: #333;
      font-weight: 700;
      font-size: 0.9rem;
      border: none;
      padding: 5px 0;
    }
    .dropdown-menu {
      margin-left: 20px;
      font-size: 0.7rem;
      padding: 8px 0;
      li {
        div {
          color: #333;
          font-weight: 700;
          font-size: 0.8rem;
          border: none;
          padding: 5px 0;
        }
      }
    }
  }

  /* Styling of overlay */
  .bm-overlay {
    background: rgba(0, 0, 0, 0.5) !important;
  }
`;
export const FormWraper = styled.div`
  padding: 90px 30px 0px 0;
`;

export const Heading = styled.div`
  font-size: 18px;
  font-weight: 500;
  margin-bottom: 25px;
  text-align: center;
`;

export const Subheading = styled.div`
  margin-bottom: 40px;
  font-size: 12px;
  font-family: "Open Sans";
  font-weight: 400;
  text-align: center;
`;
export const Text = styled.div`
  position: absolute;
  bottom: 25px;
  font-family: "Open Sans";
  font-weight: 400;
  font-size: 12px;
  span {
    color: #00b7b7;
    cursor: pointer;
  }
`;
