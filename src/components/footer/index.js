import React from "react";
import styled from "styled-components";
import { Container, Row, Col } from "react-grid-system";
import theme from "../../assets/theme/index";
import LogoIMG from "../../assets/img/logo2.svg";
import { Link } from "react-router-dom";

const FooterContainer = styled.div`
  background: ${props => theme.primaryColor};
  padding: 60px 0;
  color: ${props => theme.whiteColor};
  @media (max-width: 768px) {
    padding: 20px 30px 40px 30px;
  }
`;
const FooterWidget = styled.div``;
const Logo = styled.p`
  font-size: 2rem;
  font-weight: 500;
  max-width: 140px;
  img {
    margin-top: -60px;
  }
`;
const WidgetText = styled.p`
  font-size: 0.95rem;
  line-height: 1.5;
  font-weight: 400;
  font-family: "Open Sans", sans-serif;
  span {
    display: block;
  }
`;
const NavMenu = styled.div`
  margin-bottom: 0;
  max-width: 300px;
  @media (max-width: 768px) {
    margin-top: 40px;
  }
`;
const WidgetTitle = styled.h5`
  margin-bottom: 10px;
  font-size: 1rem;
  text-transform: uppercase;
  font-wight: 700;
  @media (max-width: 768px) {
    margin-bottom: 5px;
  }
`;
const Menu = styled.ul`
  margin: 0;
  padding: 0;
`;
const List = styled.li`
  margin-bottom: 5px;
  position: relative;
  a {
    font-size: 0.9rem;
    line-height: 1.5;
    font-weight: 700;
    text-align: left;
    @media (max-width: 768px) {
      font-size: 0.8rem;
    }
  }
`;
const Input = styled.input`
  width: 100%;
  margin: 15px 0 5px 0;
  display: block;
  border: 1px solid rgba(0, 0, 0, 0.1);
  padding: 5px 10px;
  outline: 0;
  line-height: 1;
  width: 100%;
  font-size: 0.9rem;
  font-weight: 400;
  height: 36px;
  border-radius: 3px;
  background: 0 0;
  border-color: rgba(255, 255, 255, 0.1);
  color: #ffffff;
  &::placeholder {
    color: rgba(255, 255, 255, 0.7);
  }
`;
const InputButton = styled.input`
  padding: 10px 20px;
  border-radius: 3px;
  text-transform: uppercase;
  font-size: 0.9rem;
  display: inline-block;
  vertical-align: middle;
  letter-spacing: 0.5px;
  margin-top: 10px;
  border: none;
  cursor: pointer;
  background-color: #91e1f1;
  color: #098da3;
  width: 100%;
  background: #91e1f1;
  color: #098da3;
  -webkit-appearance: none;
`;

class Footer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { email: "" };
  }
  mySubmitHandler = event => {
    event.preventDefault();
    alert("You are submitting " + this.state.username);
  };

  myChangeHandler = event => {
    this.setState({ email: event.target.value });
  };

  render() {
    return (
      <FooterContainer>
        <Container fluid style={{ maxWidth: "1200px" }}>
          <Row>
            <Col md={4} sm={12} xs={12}>
              <FooterWidget>
                <Logo>
                  <img src={LogoIMG} />
                </Logo>
                <WidgetText>
                  <span>Design & developed By · Chiranjeet Singh </span>
                  <span>Copyright 2020 · All rights reserved</span>
                </WidgetText>
              </FooterWidget>
            </Col>
            <Col md={2} sm={12} xs={12}>
              <NavMenu>
                <WidgetTitle>Destinations</WidgetTitle>
                <Menu>
                  <List>
                    <Link to="">Himachal Pradesh</Link>
                  </List>
                  <List>
                    <Link to="">Rajasthan</Link>
                  </List>
                  <List>
                    <Link to="">Uttarakhand</Link>
                  </List>
                  <List>
                    <Link to="">South India</Link>
                  </List>
                </Menu>
              </NavMenu>
            </Col>
            <Col md={2} sm={12} xs={12}>
              <NavMenu>
                <WidgetTitle>FEATURES</WidgetTitle>
                <Menu>
                  <List>
                    <Link to="/contact">Contact</Link>
                  </List>
                  <List>
                    <Link to="/author">Author</Link>
                  </List>
                  <List>
                    <Link to="/blog">My Blog</Link>
                  </List>
                  <List>
                    <Link to="/privacy-policy">Privacy Policy</Link>
                  </List>
                </Menu>
              </NavMenu>
            </Col>
            <Col md={4} sm={12} xs={12}>
              <NavMenu>
                <WidgetTitle>NEWSLETTER</WidgetTitle>
                <WidgetText>
                  Make sure to subscribe to our newsletter and be the first to
                  know the news.
                </WidgetText>
                <form onSubmit={this.mySubmitHandler}>
                  <Input
                    type="text"
                    name="email"
                    placeholder="Your email address"
                    onChange={this.myChangeHandler}
                  />
                  <InputButton type="submit" value="subscribe" />
                </form>
              </NavMenu>
            </Col>
          </Row>
        </Container>
      </FooterContainer>
    );
  }
}

export default Footer;
