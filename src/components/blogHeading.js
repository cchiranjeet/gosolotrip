import React from "react";
import styled from "styled-components";

const HeadingWrapper = styled.div`
  margin: 0 auto 50px;
  max-width: 750px;
  width: 100%;
  text-align: center;
  position: relative;
  z-index: 20;
  @media (max-width: 768px) {
    margin: 0 auto 20px;
  }
`;
const Header = styled.h1`
  color: #333333;
  margin-bottom: 0;
  line-height: 1;
  font-size: 2.6rem;
  @media (max-width: 768px) {
    font-size: 1.4rem;
  }
`;
const Title = styled.span`
  margin-top: 10px;
  display: block;
  display: block;
  font-size: 1rem;
  font-family: "Open Sans", sans-serif;
  @media (max-width: 768px) {
    font-size: 0.9rem;
  }
`;
const Paragraph = styled.p`
  margin-top: 15px;
  color: #333333;
  font-size: 1.1rem;
  font-family: "Open Sans", sans-serif;
  font-weight: 400;
  @media (max-width: 768px) {
    font-size: 1rem;
  }
`;

const BlogHeading = props => {
  return (
    <HeadingWrapper>
      <Header>{props.headingtxt}</Header>
      <Title>{props.articleno}</Title>
      <Paragraph>{props.paragraph}</Paragraph>
    </HeadingWrapper>
  );
};
export default BlogHeading;
