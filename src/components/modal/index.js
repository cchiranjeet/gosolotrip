/* eslint-disable jsx-a11y/img-redundant-alt */
import React from "react";
import styled, { keyframes } from "styled-components";
import CrossIMG from "../../assets/img/cross_black.svg";

const animatetop = keyframes`
  from {
    top: -300px;
    opacity: 0;
  }
  to {
    top: 0;
    opacity: 1;
  }
`;
const ModalContainer = styled.div`
  position: fixed; /* Stay in place */
  z-index: 99999; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0, 0, 0); /* Fallback color */
  background-color: rgba(0, 0, 0, 0.8); /* Black w/ opacity */
`;

/* Modal Content */
const ModalContent = styled.div`
  background-color: #fefefe;
  position: relative;
  margin: auto;
  line-height: 0;
  border-radius: 6px;
  width: 80%;
  max-width: ${props => props.modalwidth};
  animation-name: ${animatetop};
  animation-duration: 0.4s;
  img {
    border-radius: 6px 0 0 6px;
  }
`;
const Close = styled.span`
  color: #fff;
  display: block;
  position: absolute;
  line-height: 0;
  right: 10px;
  top: 10px;
  width: 30px;
  font-size: 60px;
  z-index: 9;
  cursor: pointer;
  img {
    transform: scale(2.5);
  }
`;
const Modal = props => {
  return (
    <ModalContainer>
      <ModalContent modalwidth={props.modalwidth}>
        <Close onClick={props.closeModal}>
          <img src={CrossIMG} alt="image gallery" />
        </Close>
        {props.children}
      </ModalContent>
    </ModalContainer>
  );
};
export default Modal;
