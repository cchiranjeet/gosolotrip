import React from "react";
import styled from "styled-components";
import theme from "../../assets/theme";
import PropTypes from "prop-types";

const BaseButton = styled.button`
  background: ${props => props.backgroundColor};
  color: ${props => props.textColor};
  font-size: 0.8rem;
  padding: 14px 30px;
  font-weight: 700;
  border-radius: 4px;
  text-transform: uppercase;
  outline: none;
  border: ${props => (props.buttonborder ? props.buttonborder : "none")};
  margin: ${props => (props.buttonmargin ? props.buttonmargin : "unset")};
  cursor: pointer;
  &:hover {
    background: ${props =>
      props.buttonborder ? "none" : "hsla( 188.57, 89.53%, 28.67%, 1)"};
    color: ${props => (props.buttonborder ? theme.primaryColor : "#ffffff")};
    border: ${props => (props.buttonborder ? "1px solid #098DA3" : "none")};
    transition: all 0.15s ease-in-out;
  }
  @media (max-width: 768px) {
    font-size: 0.7rem;
    padding: 12px 20px;
  }
`;
const StyledButton = props => {
  return (
    <BaseButton
      backgroundColor={props.backgroundColor}
      textColor={props.textColor}
      buttonborder={props.buttonborder}
      buttonmargin={props.buttonmargin}
    >
      {props.children}
    </BaseButton>
  );
};
StyledButton.propTypes = {
  backgroundColor: PropTypes.string,
  textColor: PropTypes.string,
  buttonborder: PropTypes.string,
  buttonmargin: PropTypes.string
};
export default StyledButton;
