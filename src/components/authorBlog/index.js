import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const AuthorArtcle = styled.article``;
const EntryImage = styled(Link)`
  margin-bottom: 30px;
  height: 100%;
  display: block;
  overflow: hidden;
  line-height: 0;
  &:hover {
    img {
      -webkit-transform: scale(1.1);
      -moz-transform: scale(1.1);
      -o-transform: scale(1.1);
      -ms-transform: scale(1.1);
    }
  }
  img {
    object-fit: cover;
    font-family: "object-fit: cover";
    width: 100%;
    position: relative;
    top: 0;
    left: 0;
    height: 100%;
    transition: transform 0.45s, -webkit-transform 0.45s;
    will-change: transform;
  }
`;
const TrawellEntry = styled.div`
  max-width: 600px;
  width: 100%;
  margin: 0 auto;
`;
const EntryHeader = styled.div``;
const EntryCategories = styled.div`
  pointer-events: none;
  line-height: 1;
  margin-top: -5px;
  font-size: 0;

  a {
    background-color: #d32f2f;
    font-size: 0.8rem;
    line-height: 26px;
    padding: 0 15px;
    border-radius: 15px;
    text-transform: uppercase;
    pointer-events: auto;
    display: inline-block;
    vertical-align: middle;
    margin-top: 5px;
    margin-right: 5px;
    color: #fff;
    font-family: "Open Sans";
    font-weight: 400;
  }
`;
const Tag = styled(Link)``;
const EntryTitle = styled.h3`
  margin-top: 10px;

  a {
    transition: all 0.15s ease-in-out;
    color: #333333;
    font-size: 2.2rem;
    line-height: 1.3;
  }
`;
const EntryMeta = styled.div`
  color: rgba(74, 74, 74, 1);
  margin-top: 10px;

  span {
    font-family: "Open Sans";
    font-weight: 400;
    height: 30px;
    a {
      color: rgba(74, 74, 74, 1);
    }
  }
  span + span {
    margin: 0 0 0 10px;
    &:before {
      margin-right: 10px;
      background: rgba(74, 74, 74, 0.25);
      content: "";
      width: 1px;
      height: 17px;
      display: inline-block;
      vertical-align: middle;
      margin-top: -1px;
    }
  }
`;
const MetaItem = styled.span``;
const MetaAuthor = styled.span``;
const Author = styled.span``;
const MetaComments = styled.span``;
const EntryContent = styled.div`
  margin-top: 25px;
  p {
    font-family: "Open Sans";
    font-weight: 400;
  }
`;

const AuthorBlogContent = props => {
  return (
    <AuthorArtcle>
      <EntryImage to="/">
        <img
          width={800}
          height={450}
          src="https://mksdmcdn-9b59.kxcdn.com/trawell/wp-content/uploads/2018/02/les-anderson-167377-unsplash-800x450.jpg"
          className="attachment-trawell-a1-sid size-trawell-a1-sid wp-post-image"
          alt=""
        />
      </EntryImage>
      <TrawellEntry>
        <EntryHeader>
          <EntryCategories>
            <Tag to="">Africa</Tag>
          </EntryCategories>
          <EntryTitle>
            <Link to="">Memorable Egiptian adventures on a tight budget</Link>
          </EntryTitle>
          <EntryMeta>
            <MetaItem className="metaItem">
              <span className="updated">5 days ago</span>
            </MetaItem>
            <MetaAuthor className="metaItem">
              <Author>
                <Link to="">Lindsey Hart</Link>
              </Author>
            </MetaAuthor>
            <MetaComments className="metaItem">
              <Link to="">Add comment</Link>
            </MetaComments>
          </EntryMeta>
        </EntryHeader>
        <EntryContent>
          <p>
            Maui helicopter tours are a great way to see the island from a
            different perspective and have a fun adventure. If you have never
            been on a helicopter before, this is a great place to do it. You
            will see all the beauty that Maui has to offer and can have a great
            time for the entire family. Tours are not too expensive and last
            from forty-five minutes to over an hour...
          </p>
        </EntryContent>
      </TrawellEntry>
    </AuthorArtcle>
  );
};

export default AuthorBlogContent;
