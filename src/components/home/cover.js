import React from "react";
import styled, { keyframes } from "styled-components";
import Button from "../../components/button/mediumButton";
import theme from "../../assets/theme/index";

const Breath = () => keyframes`
0% {transform: scale(1.2);}
50% {transform: scale(1);}
100% {transform: scale(1.2);}
`;
const SoloTripCover = styled.div`
  background-image: url(${props => (props.coversrc ? props.coversrc : "none")});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: 50% 50%;
  transform: scale(1.2);
  animation: ${Breath} 40s ease-in-out infinite;
  height: 100vh;
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  backface-visibility: hidden;
`;
const CoverPage = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100vh;
  overflow: hidden;
  background: ${theme.primaryColor};

  &:after {
    content: "";
    height: 250px;
    opacity: 0.7;
    width: 100%;
    z-index: 5;
    position: relative;
    background: linear-gradient(0deg, rgba(0, 0, 0, 0) 0, #000 100%);
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    pointer-events: none;
    -webkit-backface-visibility: hidden;
    @media (max-width: 768px) {
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      width: 100%;
      height: 100%;
      background: rgba(0, 0, 0, 0.5);
    }
  }
`;
const Overlay = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.5);
`;
const EntryHeader = styled.div`
  z-index: 20;
  max-width: 600px;
  color: white;
  text-align: center;
`;
const Heading = styled.h2`
  font-size: ${p => (p.fontSize ? "3.5rem" : "2.3rem")};
  text-align: center;
  margin-left: auto;
  margin-right: auto;
  line-height: 1.23;
  font-weight: 700;
  @media (max-width: 768px) {
    font-size: 1.6rem;
  }
`;
const Paragraph = styled.p`
  font-size: 1rem;
  font-family: "Open Sans", sans-serif;
  text-align: center;
  margin-top: 1rem;
  line-height: 1.6;
  font-weight: 400;
  margin-bottom: 1.3rem;
  @media (max-width: 768px) {
    font-size: 0.9rem;
    padding: 0 20px;
  }
`;

const Home = props => {
  return (
    <CoverPage>
      <SoloTripCover coversrc={props.coversrc} />
      <Overlay />
      <EntryHeader>
        <Heading fontSize={props.fontSize}>{props.heading}</Heading>
        <Paragraph>{props.subheading}</Paragraph>
        {props.Showbutton && (
          <Button
            backgroundColor={theme.primaryColor}
            textColor={theme.whiteColor}
          >
            Post your stories
          </Button>
        )}
      </EntryHeader>
    </CoverPage>
  );
};

export default Home;
