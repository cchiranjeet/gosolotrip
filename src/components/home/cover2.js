import React from "react";
import styled, { keyframes } from "styled-components";
import Button from "../../components/button/mediumButton";
import theme from "../../assets/theme/index";

const Breath = () => keyframes`
0% {transform: scale(1.2);}
50% {transform: scale(1);}
100% {transform: scale(1.2);}
`;

const TrawellCover = styled.div`
  background-color: #098da3;
  overflow: hidden;
  position: relative;
  height: 100vh;
  &:before {
    content: "";
    height: 250px;
    opacity: 0.7;
    width: 100%;
    z-index: 5;
    background: -webkit-gradient(
      linear,
      left bottom,
      left top,
      from(rgba(0, 0, 0, 0)),
      to(#000)
    );
    background: -webkit-linear-gradient(bottom, rgba(0, 0, 0, 0) 0, #000 100%);
    background: -o-linear-gradient(bottom, rgba(0, 0, 0, 0) 0, #000 100%);
    background: linear-gradient(0deg, rgba(0, 0, 0, 0) 0, #000 100%);
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    pointer-events: none;
  }
`;
const TrawellCoverItem = styled.div`
  height: 100vh;
  backface-visibility: hidden;
  position: relative;
  text-align: center;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const EntryImage = styled.div`
  display: block;
  img {
    width: 100%;
    height: 100%;
    position: absolute;
    z-index: 1;
    left: 0;
    top: 0;
    transform: scale(1.2);
    object-fit: cover;
    font-family: "object-fit: cover";
    animation: ${Breath} 40s infinite;
    vertical-align: middle;
    border-style: none;
  }
  &:after {
    background: rgba(0, 0, 0, 0.5);
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    content: "";
    display: block;
    z-index: 2;
    backface-visibility: hidden;
    -webkit-tap-highlight-color: transparent;
    -webkit-touch-callout: none;
  }
`;
const EntryHeader = styled.div`
  -webkit-tap-highlight-color: transparent;
  -webkit-touch-callout: none;
  z-index: 20;
  max-width: 800px;
  position: relative;
  backface-visibility: hidden;
  h2 {
    color: #fff;
    line-height: 1.23;
    margin-bottom: 25px;
    font-size: 2.3rem;
    @media (max-width: 768px) {
      font-size: 1.6rem;
    }
  }
`;
const TrawellEntry = styled.p`
  margin-left: auto;
  margin-right: auto;
  max-width: 600px;
  width: 100%;
  color: #fff;
  font-size: 1rem;
  font-family: "Open Sans";
  font-weight: 400;
  margin-bottom: 1.3rem;
  @media (max-width: 768px) {
    font-size: 0.9rem;
    padding: 0 20px;
  }
  &:last-child {
    margin-bottom: 0;
  }
`;

const Home = props => {
  return (
    <TrawellCover>
      <TrawellCoverItem>
        <EntryImage>
          <img width={1920} height={1254} src={props.imgUrl} alt="" />
        </EntryImage>
        <EntryHeader>
          <h2>{props.heading}</h2>
          <TrawellEntry>{props.subheading}</TrawellEntry>
          {props.Showbutton && (
            <Button
              backgroundColor={theme.primaryColor}
              textColor={theme.whiteColor}
            >
              Post your stories
            </Button>
          )}
        </EntryHeader>
      </TrawellCoverItem>
    </TrawellCover>
  );
};

export default Home;
