import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const TravelItem = styled.article`
  min-height: 180px;
  height: 100%;
  margin-bottom: 30px;
  max-height: 500px;
  position: relative;
  @media (max-width: 768px) {
    max-height: 250px;
    margin-bottom: 20px;
  }
  &:hover {
    &:after {
      height: 12px;
    }
  }
  &:after {
    content: "";
    width: 100%;
    height: 6px;
    display: block;
    position: absolute;
    bottom: -6px;
    left: 0;
    z-index: 10;
    will-change: height;
    transition: height 0.15s ease-in-out;
    background: ${props => props.BottomColor};
  }
`;
const EntryImage = styled.div`
  overflow: hidden;
  height: 100%;
  border-radius: 4px 4px 0 0;

  img {
    width: 100%;
    position: relative;
    top: 0;
    left: 0;
    height: 100%;
    -o-object-fit: cover;
    object-fit: cover;
    transition: transform 0.45s;
    transition: transform 0.45s, -webkit-transform 0.45s;
    will-change: transform;
    -webkit-backface-visibility: hidden;
    -webkit-transform: translate3d(0, 0, 0);
  }
  &:hover {
    img {
      -webkit-transform: scale(1.1);
      -moz-transform: scale(1.1);
      -o-transform: scale(1.1);
      -ms-transform: scale(1.1);
    }
  }
  &:after {
    content: "";
    background: linear-gradient(180deg, rgba(0, 0, 0, 0) 0, #000 100%);
    height: 60%;
    top: auto;
    bottom: 0;
    backface-visibility: hidden;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    content: "";
    display: block;
    z-index: 2;
    -webkit-tap-highlight-color: transparent;
    -webkit-touch-callout: none;
  }
`;
const EntryHeader = styled.div`
  position: absolute;
  z-index: 3;
  bottom: 40px;
  left: 40px;
  pointer-events: none;
  @media (max-width: 768px) {
    bottom: 20px;
    left: 30px;
  }
`;
const EntryTitle = styled.h3`
  margin-bottom: 0;
  text-transform: none;
  font-size: 1.8rem;
  @media (max-width: 768px) {
    font-size: 1.2rem;
  }
  a {
    transition: all 0.15s ease-in-out;
    color: #fff;
  }
`;
const EntryMeta = styled.div`
  font-size: 1.3rem;
  color: rgba(255, 255, 255, 0.8);
  pointer-events: auto;
  @media (max-width: 768px) {
    font-size: 1rem;
  }
`;

const TopDestination = props => {
  return (
    <TravelItem BottomColor={props.BottomColor}>
      <Link to={props.url}>
        <EntryImage>
          <img src={props.src} alt={props.alt} />
        </EntryImage>
      </Link>
      <EntryHeader>
        <EntryTitle>
          <Link to="">{props.Heading}</Link>
        </EntryTitle>
        <EntryMeta>{props.subheading}</EntryMeta>
      </EntryHeader>
    </TravelItem>
  );
};

export default TopDestination;
