import React, { Component } from "react";
import styled from "styled-components";
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";

const MapWrapper = styled.div`
  position: relative;
  height: 100vh;
  width: 100%;
  border-radius: 4px;
  overflow: hidden;
  @media (max-width: 768px) {
    height: 70vh;
  }
`;
const MapView = styled(Map)`
  height: 100vh;
  position: relative;
  width: 100%;
  @media (max-width: 768px) {
    height: 70vh;
  }
`;
const MaptDetails = styled.div`
  padding: 0px 20px 20px;
`;
const EntryCategory = styled.div`
  margin-top: 10px;
  margin-bottom: 7px;
  a {
    color: #ab47bc;
    display: block;
    font-family: "Open Sans", sans-serif !important;
    font-weight: 400 !important;
  }
`;
const EntryHeader = styled.div``;
const EntryTitle = styled.h6`
  a {
    font-size: 1rem;
    color: #333;
    line-height: 1.4;
    &:hover {
      color: #00b7b7;
    }
  }
`;
const EntryMeta = styled.div`
  margin-top: 3px;
  font-family: "Open Sans", sans-serif !important;
  font-weight: 400 !important;
`;
const Meta = styled.span`
  font-size: 12px;
`;
const EntryContent = styled.div`
  margin-top: 15px;
  p {
    font-size: 13px;
    line-height: 1.6;
    font-family: "Open Sans", sans-serif;
    font-weight: 400;
  }
`;

class WithMarkers extends Component {
  state = {
    activeMarker: {},
    selectedPlace: {},
    showingInfoWindow: false
  };

  onMarkerClick = (props, marker) =>
    this.setState({
      activeMarker: marker,
      selectedPlace: props,
      showingInfoWindow: true
    });

  onInfoWindowClose = () =>
    this.setState({
      activeMarker: null,
      showingInfoWindow: false
    });

  onMapClicked = () => {
    if (this.state.showingInfoWindow)
      this.setState({
        activeMarker: null,
        showingInfoWindow: false
      });
  };

  render() {
    if (!this.props.loaded) return <div>Loading...</div>;
    console.log(this.props);
    return (
      <MapWrapper>
        <MapView
          className="map"
          google={this.props.google}
          onClick={this.onMapClicked}
          initialCenter={this.props.initialCenter}
          zoom={this.props.zoom}
          gestureHandling="none"
        >
          {this.props.markers.map((marker, index) => {
            return (
              <Marker
                position={marker.position}
                name={marker.name}
                key={index}
                onClick={this.onMarkerClick}
              />
            );
          })}

          <InfoWindow
            marker={this.state.activeMarker}
            onClose={this.onInfoWindowClose}
            visible={this.state.showingInfoWindow}
          >
            <div>
              <div>
                <img
                  src="https://demo.mekshq.com/trawell/wp-content/uploads/2018/02/clem-onojeghuo-111588-unsplash-380x214.jpg"
                  alt=""
                />
              </div>
              <MaptDetails>
                <EntryCategory>
                  <a href="#" class="cat-7">
                    {this.state.selectedPlace.name}
                  </a>
                  <EntryHeader>
                    <EntryTitle>
                      <a href="">
                        New ideas for a low cost vacation in the Caribbean
                      </a>
                    </EntryTitle>
                  </EntryHeader>
                  <EntryMeta>
                    <Meta>
                      <span class="updated">1 month ago</span>
                    </Meta>
                  </EntryMeta>
                  <EntryContent>
                    <p>One of the best ways to make a great vacation...</p>
                  </EntryContent>
                </EntryCategory>
              </MaptDetails>
            </div>
          </InfoWindow>
        </MapView>
      </MapWrapper>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "" //AIzaSyBP7OIZQu9m4R0ygdBR7qlupTtF18bDaJU
})(WithMarkers);
