import React from "react";
import styled from "styled-components";
import mapboxgl from "mapbox-gl";
import MarkerIcon from "../../assets/img/marker.png";
mapboxgl.accessToken =
  "pk.eyJ1IjoiY2NoaXJhbmplZXQiLCJhIjoiY2s1ODZpcDRpMGthMDNwbGo3cnQ3eHRpZiJ9.no9Fcx9MVfcEZD8hDgdRTg";
const MapWrapper = styled.div`
  position: relative;
  overflow: hidden;
  height: 100vh;
`;
const MapContainer = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
  height: 100vh;
`;

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lng: 78.9629,
      lat: 20.5937,
      zoom: 6.5
    };
  }
  componentDidMount() {
    const map = new mapboxgl.Map({
      container: this.mapContainer,
      style: "mapbox://styles/mapbox/streets-v11?optimize=true",
      center: [this.state.lng, this.state.lat],
      zoom: this.state.zoom
    });

    map.on("load", function() {
      /* Image: An image is loaded and added to the map. */
      map.loadImage(MarkerIcon, function(error, image) {
        if (error) throw error;
        map.addImage("custom-marker", image);
        /* Style layer: A style layer ties together the source and image and specifies how they are displayed on the map. */
        map.addLayer({
          id: "markers",
          type: "symbol",
          /* Source: A data source specifies the geographic coordinate where the image marker gets placed. */
          source: {
            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: [
                {
                  type: "Feature",
                  properties: {},
                  geometry: {
                    type: "Point",
                    coordinates: [78.7277, 21.9786]
                  },
                  geometry: {
                    type: "Point",
                    coordinates: [75.7277, 20.9786]
                  }
                }
              ]
            }
          },
          layout: {
            "icon-image": "custom-marker"
          }
        });
      });
    });
    map.on("move", () => {
      this.setState({
        lng: map.getCenter().lng.toFixed(4),
        lat: map.getCenter().lat.toFixed(4),
        zoom: map.getZoom().toFixed(2)
      });
    });
    map.scrollZoom.disable();
  }

  render() {
    return (
      <MapWrapper>
        <MapContainer ref={el => (this.mapContainer = el)} />
      </MapWrapper>
    );
  }
}

export default Home;
