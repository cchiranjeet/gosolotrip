import React, { Component } from "react";
import PropTypes from "prop-types";

import Tab from "./tab";
import styled from "styled-components";

const TabStyle = styled.div`
  margin-bottom: 20px;

  .tab-list {
    border-bottom: 2px solid rgba(51, 51, 51, 0.1);
    padding-left: 0;
    /* display: flex; */
    /* position: sticky; */
    /* top: 80px; */
    z-index: 9;
    background: #fff;
    justify-content: center;
  }

  .tab-list-item {
    display: inline-block;
    list-style: none;
    margin-bottom: -1px;
    cursor: pointer;
    font-weight: 700;
    padding: 10px 10px 10px 0;
    margin-right: 32px;
    text-align: center;
    /* flex-grow: 1; */
  }

  .tab-list-active {
    background-color: white;
    border-bottom: 3px solid #00b7b7;
  }
  .tab-content {
    font-weight: 400;
    padding: 20px 0 0px 0;
    min-height: 300px;
    font-family: "Open Sans", sans-serif;
    .view {
      font-size: 14px;
      font-family: "Quicksand", sans-serif;
      float: right;
      font-weight: 600;
      cursor: pointer;
      color: #00b7b7;
      padding-top: 1px;
      &:hover {
        svg {
          transform: translateX(2px);
        }
      }
      svg {
        width: 20px;
        height: 20px;
        transition: all 0.2s ease-in-out;
        vertical-align: middle;
        path {
          fill: rgb(0, 183, 183) !important;
        }
      }
    }
    .rotated {
      /* writing-mode: tb-rl; */
      /* transform: rotate(-180deg); */
      /* float: left; */
      font-family: "Quicksand", sans-serif;
      font-size: 16px;
      color: #333;
      font-weight: 700;
    }
  }
`;
class Tabs extends Component {
  static propTypes = {
    children: PropTypes.instanceOf(Array).isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      activeTab: this.props.children[0].props.label
    };
  }

  onClickTabItem = tab => {
    this.setState({ activeTab: tab });
  };

  render() {
    const {
      onClickTabItem,
      props: { children },
      state: { activeTab }
    } = this;

    return (
      <TabStyle>
        <div className="tabs">
          <ol className="tab-list">
            {children.map(child => {
              const { label } = child.props;

              return (
                <Tab
                  activeTab={activeTab}
                  key={label}
                  label={label}
                  onClick={onClickTabItem}
                />
              );
            })}
          </ol>
          <div className="tab-content">
            {children.map(child => {
              if (child.props.label !== activeTab) return undefined;
              return child.props.children;
            })}
          </div>
        </div>
      </TabStyle>
    );
  }
}

export default Tabs;
