import React from "react";
import "swiper/css/swiper.css";
import styled from "styled-components";
import Errorpage from "../../pages/404";
import Home from "../../pages/home";
import Privacy from "../../pages/privacyPolicy";
import TopHeader from "../../components/header";
// import Inspirations from "../../pages/inspirations";
// import InspirationBlogView from "../../pages/blogView/inspirationView";
import Destination from "../../pages/destinations";
import Inspirations from "../../pages/inspirations";

import DestinationBlogView from "../../pages/blogView";
import PublishStories from "../../pages/postStories/createNew";
import Footer from "../../components/footer";
import TripPreFooter from "../../components/tripPreFooter";
import AuthorBlog from "../../pages/author";
import AboutAuthor from "../../pages/author/aboutAuthor";
import GlobalStyles from "../../assets/style/globalStyle";
import Contact from "../../pages/contact";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
// Create a Wrapper component that'll render a <section> tag with some styles
const Wrapper = styled.section``;

const RouteWithProps = ({
  path,
  exact,
  strict,
  component: Component,
  location,
  ...rest
}) => (
  <Route
    path={path}
    exact={exact}
    strict={strict}
    location={location}
    render={props => <Component {...props} {...rest} />}
  />
);
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      headerTheme: false,
      loggedIn: JSON.parse(localStorage.getItem("user")) || false
    };
  }

  changeHeaderTheme = event => {
    this.setState({
      headerTheme: event
    });
  };

  setLoggedIn = loginStatus => {
    this.setState({ loggedIn: loginStatus });
  };

  render() {
    return (
      <Router>
        <Wrapper>
          <GlobalStyles />
          <TopHeader
            loggedIn={this.state.loggedIn}
            setLoggedIn={this.setLoggedIn}
            light={this.state.headerTheme}
          />
          <Switch>
            <Route
              exact
              path="/"
              render={props => (
                <Home
                  {...props}
                  changeHeaderTheme={event => this.changeHeaderTheme(event)}
                />
              )}
            />
            <Route
              exact
              path="/blog"
              render={props => (
                <AuthorBlog
                  {...props}
                  changeHeaderTheme={this.changeHeaderTheme}
                />
              )}
            />

            <Route
              exact
              path="/contact"
              render={props => (
                <Contact
                  {...props}
                  changeHeaderTheme={this.changeHeaderTheme}
                />
              )}
            />

            <Route
              exact
              path="/author"
              render={props => (
                <AboutAuthor
                  {...props}
                  changeHeaderTheme={this.changeHeaderTheme}
                />
              )}
            />

            <Route
              exact
              path="/publish-stories"
              render={props => (
                <PublishStories
                  {...props}
                  changeHeaderTheme={this.changeHeaderTheme}
                />
              )}
            />

            <Route
              exact
              path="/explore/:category"
              render={props => (
                <Inspirations
                  {...props}
                  changeHeaderTheme={this.changeHeaderTheme}
                />
              )}
            />
            <Route
              exact
              path="/place/:category"
              render={props => (
                <Destination
                  {...props}
                  changeHeaderTheme={this.changeHeaderTheme}
                />
              )}
            />
            <Route
              exact
              path="/privacy-policy"
              render={props => (
                <Privacy
                  {...props}
                  changeHeaderTheme={this.changeHeaderTheme}
                />
              )}
            />

            <Route
              exact
              path="/explore/:category/:id"
              render={props => (
                <DestinationBlogView
                  {...props}
                  changeHeaderTheme={this.changeHeaderTheme}
                />
              )}
            />
            <RouteWithProps component={Errorpage} />
          </Switch>
          <TripPreFooter />
          <Footer />
        </Wrapper>
      </Router>
    );
  }
}

export default App;
