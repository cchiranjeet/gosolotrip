import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const ParagraphSection = styled.div`
  margin-bottom: 25px;
  ul {
    list-style: disc;
    margin-left: 20px;
    li {
      font-size: 1rem;
      font-family: "Open Sans", sans-serif;
      font-weight: 400;
      color: #333;
    }
  }
`;

const ParagraphWithList = props => {
  console.log(props.content);
  return (
    <>
      {props.content.map((data, index) => {
        return (
          <ParagraphSection key={index}>
            <ul>
              {data.list.map((para, indexItem) => {
                return <li key={indexItem}>{para}</li>;
              })}
            </ul>
          </ParagraphSection>
        );
      })}
    </>
  );
};

export default ParagraphWithList;
