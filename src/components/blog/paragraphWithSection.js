import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const ParagraphSection = styled.div`
  margin-bottom: 25px;
`;
const Heading = styled.div`
  font-size: 1.4rem;
  margin-bottom: 5px;
  font-weight: 600;
  color: #333;
`;
const Text = styled.div`
  font-size: 1rem;
  font-family: "Open Sans", sans-serif;
  font-weight: 400;
  color: #333;
`;

const paragraphWithSection = props => {
  return (
    <>
      {props.content.map(data => (
        <ParagraphSection>
          <Heading>{data.heading}</Heading>
          <Text>{data.text}</Text>
        </ParagraphSection>
      ))}
    </>
  );
};

export default paragraphWithSection;
