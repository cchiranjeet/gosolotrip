import React from "react";
import styled from "styled-components";

const Heading = styled.div`
  font-size: 1.4rem;
  font-weight: 600;
  color: #333;
`;

const paragraphWithSection = props => {
  return (
    <>
      <Heading>{props.text}</Heading>
    </>
  );
};

export default paragraphWithSection;
