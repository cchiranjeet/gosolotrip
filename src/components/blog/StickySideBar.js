/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-script-url */
import React from "react";
import styled from "styled-components";
import { withRouter } from "react-router-dom";
import Photo from "../../assets/img/photo.jpg";
import {
  FacebookShareButton,
  RedditShareButton,
  WhatsappShareButton,
  EmailShareButton,
  PinterestShareButton,
  TwitterShareButton
} from "react-share";

const StyckyHeader = styled.div`
  position: -webkit-sticky;
  position: sticky;
  top: 100px;
`;
const TravelSidebarSticky = styled.div`
  position: relative;
  padding-bottom: 30px;
`;
const WidgetMini = styled.div`
  margin-bottom: 17px;
  &:after {
    background: rgba(51, 51, 51, 0.1);
    content: "";
    width: 20px;
    height: 1px;
    display: block;
    margin-top: 20px;
  }
  img {
    border-radius: 2px;
    vertical-align: middle;
    border-style: none;
    max-width: 150px;
    height: auto;
  }
  &:last-child {
    &:after {
      display: none;
    }
  }
`;
const EntryMetaSmall = styled.span`
  margin-top: 10px;
  opacity: 0.5;
  font-size: 0.8rem;
  display: block;
  font-family: "Open Sans";
  font-weight: 400;
`;
const EntryMetaAuther = styled.a`
  transition: all 0.15s ease-in-out;
  color: #333333;
  font-weight: 700;
  margin-top: -5px;
  display: block;
`;
const TravelShare = styled.div``;
const Box = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: flex-start;
  margin-top: 5px;
  margin-left: -3px;
  margin-right: -3px;
  margin-bottom: -3px;
  button {
    outline: none;
  }
  span {
    flex: 0 0 48px;
    width: 48px !important;
    text-align: center;
    font-size: 0.9rem;
    display: inline-block;
    outline: none;
    line-height: 26px;
    border-radius: 3px !important;
    margin-bottom: 3px;
    margin-left: 3px;
    margin-top: 0 !important;
    margin-right: 0;
    min-width: auto !important;
    height: auto;
    transition: all 0.15s ease-in-out;
    border: 1px solid rgba(0, 0, 0, 0.1);
    border-color: rgba(51, 51, 51, 0.1);
    background: transparent !important;

    :before {
      font-family: "socicon" !important;
      font-style: normal !important;
      font-size: 14px;
      font-weight: normal !important;
      font-variant: normal !important;
      text-transform: none !important;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
      display: inline-block;
      font-size: inherit;
      text-rendering: auto;
    }
    span {
      display: none;
    }
    &.socicon-facebook {
      content: "\e028";
      color: #3e5b98;
    }
    &.socicon-twitter {
      content: "\e08d";
      color: #4da7de;
    }
    &.socicon-reddit {
      content: "\e06c";
      color: #e74a1e;
    }
    &.socicon-pinterest {
      content: "\e063";
      color: #c92619;
    }
    &.socicon-mail {
      color: #000000;
      content: "\e050";
    }
    &.socicon-whatsapp {
      color: #20b038;
      content: "\e099";
    }
  }
`;
const Nav = styled.nav`
  display: block;
`;
const TravelNextLink = styled.div`
  display: block;
  a {
    color: #333333;
    :hover {
      color: #098da3;
    }
  }
`;
const TravelNextPrevLink = styled.span`
  display: block;
  transition: all 0.15s ease-in-out;
  line-height: 1.4;
  font-size: 0.9rem;
  font-weight: 700;
`;

class StickySideBar extends React.Component {
  render() {
    const url = window.location;
    const title = "GoSoloTrip";

    return (
      <StyckyHeader>
        <TravelSidebarSticky>
          <WidgetMini>
            <img alt="" src={Photo} className="avatar avatar-150 photo" />
            <EntryMetaSmall>Written by</EntryMetaSmall>
            <EntryMetaAuther href="https://demo.mekshq.com/trawell/?author=2">
              Chiranjeet Singh
            </EntryMetaAuther>
          </WidgetMini>
          <WidgetMini>
            <EntryMetaSmall>Share this article</EntryMetaSmall>
            <TravelShare>
              <Box>
                <FacebookShareButton url={url} quote={title}>
                  <span className="socicon-facebook">
                    <span>Facebook</span>
                  </span>
                </FacebookShareButton>
                <TwitterShareButton url={url} quote={title}>
                  <span className="meks_ess-item socicon-twitter">
                    <span>Twitter</span>
                  </span>
                </TwitterShareButton>
                <RedditShareButton url={url} quote={title}>
                  <span className="meks_ess-item socicon-reddit">
                    <span>Reddit</span>
                  </span>
                </RedditShareButton>
                <PinterestShareButton
                  media="http://127.0.0.1:3000/static/media/jaipur.28ce345f.jpg"
                  description="hi"
                >
                  <span className="meks_ess-item socicon-pinterest">
                    <span>Pinterest</span>
                  </span>
                </PinterestShareButton>
                <EmailShareButton url={url} quote={title}>
                  <span className="socicon-mail prevent-share-popup ">
                    <span>Email</span>
                  </span>
                </EmailShareButton>
                <WhatsappShareButton url={url} quote={title}>
                  <span className="meks_ess-item socicon-whatsapp prevent-share-popup">
                    <span>WhatsApp</span>
                  </span>
                </WhatsappShareButton>
              </Box>
            </TravelShare>
          </WidgetMini>
          <WidgetMini>
            <Nav>
              <TravelNextLink>
                <EntryMetaSmall>Next article</EntryMetaSmall>
                <a href="" rel="next">
                  <TravelNextPrevLink>
                    What to pack when traveling to Barcelona
                  </TravelNextPrevLink>
                </a>
              </TravelNextLink>
            </Nav>
          </WidgetMini>
        </TravelSidebarSticky>
      </StyckyHeader>
    );
  }
}

export default withRouter(StickySideBar);
