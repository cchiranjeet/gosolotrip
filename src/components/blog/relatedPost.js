import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const TravePost = styled.div`
  background: #ffffff;
  padding: 25px 0;
  margin-bottom: 0px;
  @media (max-width: 768px) {
    padding: 0px;
  }
`;
const WidgetTitle = styled.a`
  background: transparent;
  color: #7cb342;
  padding: 0;
  margin: 0 6px 0 0;
  font-size: 0.8rem;
  font-family: "Open Sans";
  font-weight: 400;
  line-height: 2;
  width: auto;
  text-transform: uppercase;
  pointer-events: auto;
  display: inline-block;
`;
const TripItem = styled.article`
  display: flex;
  margin-bottom: 20px;
`;
const EntryHeader = styled.div`
  margin-top: -5px;
  line-height: 1.4;
  text-align: left;
`;
const EntryIMage = styled(Link)`
  margin-right: 10px;
  flex: 0 0 230px;
  height: auto;
  overflow: hidden;
  @media (max-width: 768px) {
    flex: 0 0 130px;
  }
  img {
    object-fit: cover;
    font-family: "object-fit: cover";
    width: 100%;
    position: relative;
    transition: transform 0.45s, -webkit-transform 0.45s;
    will-change: transform;
    top: 0;
    left: 0;
    height: auto;
    border-radius: 2px;
  }
`;
const MetaItem = styled.span`
  height: 30px;
  text-align: left;
`;
const EntryMeta = styled.div`
  margin-top: 8px;
  color: rgba(74, 74, 74, 1);
  font-size: 0.8rem;
  text-align: left;
`;
const EntryTitle = styled(Link)`
  transition: all 0.15s ease-in-out;
  color: #333333;
  line-height: 1.2;
  font-weight: 700;
  color: #333333;
  font-size: 1.4rem;
  text-align: left;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  &:hover {
    color: #098da3;
  }
  @media (max-width: 768px) {
    font-size: 0.9rem;
  }
`;

const RelatedPost = props => {
  return (
    <TravePost>
      <TripItem>
        <EntryIMage to="">
          <img
            src="https://mksdmcdn-9b59.kxcdn.com/trawell/wp-content/uploads/2018/02/brevite-434280-unsplash-1200x675.jpg"
            alt=""
          />
        </EntryIMage>
        <EntryHeader>
          <WidgetTitle>Delhi</WidgetTitle>
          <EntryTitle to="">
            From the land of smiles to the kingdom of wonder
          </EntryTitle>
          <EntryMeta>
            <MetaItem>
              <span className="updated">2 days ago</span>
            </MetaItem>
          </EntryMeta>
        </EntryHeader>
      </TripItem>
      <TripItem>
        <EntryIMage to="">
          <img
            src="https://mksdmcdn-9b59.kxcdn.com/trawell/wp-content/uploads/2018/02/brevite-434280-unsplash-1200x675.jpg"
            alt=""
          />
        </EntryIMage>
        <EntryHeader>
          <WidgetTitle>Delhi</WidgetTitle>

          <EntryTitle to="">Tips for renting a car in Namibia</EntryTitle>
          <EntryMeta>
            <MetaItem>
              <span className="updated">2 days ago</span>
            </MetaItem>
          </EntryMeta>
        </EntryHeader>
      </TripItem>
      <TripItem>
        <EntryIMage to="">
          <img
            src="https://mksdmcdn-9b59.kxcdn.com/trawell/wp-content/uploads/2018/02/brevite-434280-unsplash-1200x675.jpg"
            alt=""
          />
        </EntryIMage>
        <EntryHeader>
          <WidgetTitle>Delhi</WidgetTitle>

          <EntryTitle to="">
            Memorable Egiptian adventures on a tight budget
          </EntryTitle>
          <EntryMeta>
            <MetaItem>
              <span className="updated">2 days ago</span>
            </MetaItem>
          </EntryMeta>
        </EntryHeader>
      </TripItem>
    </TravePost>
  );
};

export default RelatedPost;
