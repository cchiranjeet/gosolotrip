import React from "react";
import styled from "styled-components";

const ParagraphWithDay = styled.div`
  margin-bottom: 25px;
`;
const Title = styled.div`
  font-size: 0.9rem;
  font-weight: 700;
  font-style: italic;
`;
const Text = styled.div`
  margin-bottom: 10px;
  font-family: "Open Sans", sans-serif;
  font-weight: 400;
  font-size: 1rem;
`;

const ParagraphDay = props => {
  return (
    <>
      {props.content.map(data => (
        <ParagraphWithDay>
          <Title>{data.title}</Title>
          <Text>{data.text}</Text>
          <img src={data.imgUrl} alt="" />
        </ParagraphWithDay>
      ))}
    </>
  );
};

export default ParagraphDay;
