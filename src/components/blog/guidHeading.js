import React from "react";
import styled from "styled-components";

const Title = styled.div`
  font-size: 0.9rem;
  font-weight: 700;
  margin-bottom: 5px;
  font-style: italic;
`;
const GuidHeading = props => {
  return (
    <>
      <Title>{props.title}</Title>
    </>
  );
};

export default GuidHeading;
