import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const TravelPost = styled.article`
  margin-bottom: 40px;
  overflow: hidden;
  @media (max-width: 768px) {
    margin-bottom: 30px;
  }
`;
const EntryImage = styled(Link)`
  margin-bottom: 20px;
  height: 100%;
  line-height: 0;
  display: block;
  overflow: hidden;
  img {
    object-fit: cover;
    font-family: "object-fit: cover";
    border-radius: 4px;
    width: 100%;
    position: relative;
    top: 0;
    left: 0;
    height: 100%;
    transition: transform 0.45s, -webkit-transform 0.45s;
    will-change: transform;
  }
  &:hover {
    img {
      -webkit-transform: scale(1.1);
      -moz-transform: scale(1.1);
      -o-transform: scale(1.1);
      -ms-transform: scale(1.1);
    }
  }
`;

const EntryHeader = styled.div`
  margin-top: 15px;

  @media (max-width: 768px) {
    margin-top: 10px;
  }
`;
const Entrycategory = styled.div`
  pointer-events: none;
  line-height: 1;
  margin-top: -5px;
  font-size: 0;
  font-family: "Open Sans", sans-serif;
  a {
    font-size: 0.8rem;
    line-height: 26px;
    padding: 0 15px;
    color: #fff;
    border-radius: 15px;
    text-transform: uppercase;
    pointer-events: auto;
    display: inline-block;
    vertical-align: middle;
    margin-top: 5px;
    margin-right: 5px;
    transition: all 0.15s ease-in-out;
    background-color: #7cb342;
  }
`;
const EntryTitle = styled.h3`
  margin-top: 10px;
  line-height: 1.31;
  font-size: 2rem;
  a {
    color: #333;
    transition: all 0.15s ease-in-out;
    &:hover {
      color: #098da3;
    }
  }
  @media (max-width: 768px) {
    font-size: 1.4rem;
  }
`;
const EntryMeta = styled.div`
  margin-top: 10px;
`;
const MetaItem = styled.span`
  height: 30px;
  span {
    color: rgba(74, 74, 74, 1);
    height: 30px;
    font-size: 0.9rem;
    line-height: 1.3;
    font-weight: 500;
  }
`;
const MetaComent = styled.span`
  margin: 0 0 0 10px;
  a {
    height: 30px;
    color: rgba(74, 74, 74, 1);
    font-size: 0.9rem;
    line-height: 1.3;
    font-weight: 500;
    &:hover {
      color: #098da3;
    }
  }
  &:before {
    content: "";
    width: 1px;
    height: 17px;
    display: inline-block;
    background: rgba(74, 74, 74, 0.25);
    vertical-align: middle;
    margin-right: 15px;
    margin-top: -1px;
    margin-right: 10px;
  }
`;
const EntryContent = styled.div`
  margin-top: 15px;
  @media (max-width: 768px) {
    margin-top: 10px;
  }
`;
const Paragraph = styled.p`
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 3;
  font-size: 1rem;
  font-family: "Open Sans";
  font-weight: 400;
  -webkit-box-orient: vertical;
  @media (max-width: 768px) {
    font-size: 0.9rem;
  }
`;

const BlogTile = props => {
  return (
    <TravelPost onClick={props.toBlogView}>
      {/* <Link to={props.url} className="entryImage"> */}

      <EntryImage to="#">
        <img src={props.imagesrc} alt="" />
      </EntryImage>
      {/* </Link> */}
      <EntryHeader>
        <Entrycategory>
          <Link to="/">{props.location}</Link>
        </Entrycategory>
        <EntryTitle>
          <Link to={props.url}>{props.title}</Link>
        </EntryTitle>
        <EntryMeta>
          <MetaItem>
            <span className="updated">{props.lastdateofpost}</span>
          </MetaItem>
          <MetaComent>
            <Link to="/">{props.comment}</Link>
          </MetaComent>
        </EntryMeta>
      </EntryHeader>

      <EntryContent>
        <Paragraph>{props.content}</Paragraph>
      </EntryContent>
    </TravelPost>
  );
};

export default BlogTile;
