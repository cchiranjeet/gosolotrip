import React from "react";
import styled from "styled-components";

const EntryContent = styled.div`
  margin-bottom: 25px;
  @media (max-width: 768px) {
    margin-top: 15px;
  }
`;
const Paragraph = styled.p`
  font-family: "Open Sans", sans-serif;
  font-weight: 400;
  margin-bottom: 1.5rem;
  color: #333333;
  font-size: 1rem;
  &:last-child {
    margin-bottom: 0;
  }
  @media (max-width: 768px) {
    font-size: 0.8rem;
  }
`;

const BlogContent = props => {
  return (
    <EntryContent>
      <Paragraph>{props.content}</Paragraph>
    </EntryContent>
  );
};

export default BlogContent;
