import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const TravePost = styled.div`
  background: #ffffff;
  padding: 25px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.2);
  margin-bottom: 50px;
`;
const WidgetTitle = styled.h4`
  text-transform: uppercase;
  font-size: 1rem;
  line-height: 1.3;
  margin-bottom: 20px;
  text-align: left;
  color: #333333;
`;
const TripItem = styled.article`
  display: flex;
  margin-bottom: 20px;
`;
const EntryHeader = styled.div`
  margin-top: -5px;
  line-height: 1.4;
  text-align: left;
`;
const EntryIMage = styled(Link)`
  margin-right: 10px;
  width: 60px;
  flex: 0 0 60px;
  height: 100%;
  overflow: hidden;
  img {
    object-fit: cover;
    font-family: "object-fit: cover";
    width: 100%;
    position: relative;
    top: 0;
    left: 0;
    height: 100%;
  }
`;
const MetaItem = styled.span`
  height: 30px;
  text-align: left;
`;
const EntryMeta = styled.div`
  margin-top: 3px;
  color: rgba(74, 74, 74, 1);
  font-size: 0.8rem;
  text-align: left;
`;
const EntryTitle = styled(Link)`
  transition: all 0.15s ease-in-out;
  color: #333333;
  line-height: 1.4;
  font-weight: 700;
  color: #333333;
  font-size: 0.9rem;
  text-align: left;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  &:hover {
    color: #098da3;
  }
`;

const ResentStories = props => {
  return (
    <TravePost>
      <WidgetTitle>Recent Stories</WidgetTitle>
      <TripItem>
        <EntryIMage to="">
          <img
            width={150}
            height={150}
            src="https://mksdmcdn-9b59.kxcdn.com/trawell/wp-content/uploads/2018/03/heping-271656-unsplash-150x150.jpg"
            alt=""
          />
        </EntryIMage>
        <EntryHeader>
          <EntryTitle to="">
            From the land of smiles to the kingdom of wonder
          </EntryTitle>
          <EntryMeta>
            <MetaItem>
              <span className="updated">2 days ago</span>
            </MetaItem>
          </EntryMeta>
        </EntryHeader>
      </TripItem>
      <TripItem>
        <EntryIMage to="">
          <img
            width={150}
            height={150}
            src="https://mksdmcdn-9b59.kxcdn.com/trawell/wp-content/uploads/2018/03/james-ballard-112688-unsplash-150x150.jpg"
            className="attachment-thumbnail size-thumbnail wp-post-image"
            alt=""
          />
        </EntryIMage>
        <EntryHeader>
          <EntryTitle to="">Tips for renting a car in Namibia</EntryTitle>
          <EntryMeta>
            <MetaItem>
              <span className="updated">2 days ago</span>
            </MetaItem>
          </EntryMeta>
        </EntryHeader>
      </TripItem>
      <TripItem>
        <EntryIMage to="">
          <img
            width={150}
            height={150}
            src="https://mksdmcdn-9b59.kxcdn.com/trawell/wp-content/uploads/2018/02/les-anderson-167377-unsplash-150x150.jpg"
            className="attachment-thumbnail size-thumbnail wp-post-image"
            alt=""
          />
        </EntryIMage>
        <EntryHeader>
          <EntryTitle to="">
            Memorable Egiptian adventures on a tight budget
          </EntryTitle>
          <EntryMeta>
            <MetaItem>
              <span className="updated">2 days ago</span>
            </MetaItem>
          </EntryMeta>
        </EntryHeader>
      </TripItem>
    </TravePost>
  );
};

export default ResentStories;
