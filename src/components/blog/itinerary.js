import React from "react";
import { Container, Row, Col } from "react-grid-system";
import styled from "styled-components";

const ItineraryWarpper = styled.div`
  .brief_intro {
    margin-top: 40px;

    div {
      padding-bottom: 30px;
      position: relative;
    }
    .day {
      color: #333;
      font-size: 14px;
      font-weight: 500;
      margin-top: -6px;
      /* font-style: italic; */
      display: block;

      span {
        display: block;
        color: #00b7b7;
        font-weight: 300;
      }
    }
    .orange {
      color: #00b7b7;
      text-align: justify;
      line-height: 1.3;
      word-wrap: break-word;
    }
    .trip_brief {
      color: #333;
      font-size: 14px;
      font-family: "Open Sans", sans-serif;
      font-weight: 600;
      margin-top: -7px;
      display: block;
      .trip_extra {
        font-weight: 400;
        display: block;
      }
    }
    .left_border {
      border-left: 2px solid rgba(51, 51, 51, 0.1);
      &::before {
        content: "";
        width: 10px;
        height: 10px;
        background: #00b7b7;
        position: absolute;
        left: -6px;
        border-radius: 100rem;
      }

      &:last-child {
        border-left: none !important;
        left: 2px !important;
      }
    }
  }
`;

const Itinerary = props => {
  return (
    <ItineraryWarpper>
      <Container fluid>
        <Row className="brief_intro">
          <Col md={2}>
            <div className="day">Day 1</div>
          </Col>
          <Col md={9} className="left_border">
            <span className="trip_brief">
              Arrival in Jaipur{" "}
              {/* <span className="trip_extra">Accommodation: Hawa Mahal</span> */}
            </span>
          </Col>
          <Col md={2}>
            <div className="day">Day 2</div>
          </Col>
          <Col md={9} className="left_border">
            <span className="trip_brief">
              City Tour of Amer / Jaychand fort
              {/* <span className="trip_extra">Accommodation: Hawa Mahal</span> */}
            </span>
          </Col>
          <Col md={2}>
            <div className="day">Day 3</div>
          </Col>
          <Col md={9} className="left_border">
            <span className="trip_brief">
              Arrival in Jaipur{" "}
              {/* <span className="trip_extra">Accommodation: Hawa Mahal</span> */}
            </span>
          </Col>
        </Row>
      </Container>
    </ItineraryWarpper>
  );
};
export default Itinerary;
