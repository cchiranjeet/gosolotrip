import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const TravelPost = styled.article`
  position: relative;
  margin-bottom: 25px;
  overflow: hidden;
  @media (max-width: 768px) {
    margin-bottom: 20px;
  }
`;
const EntryHeader = styled.div``;
const Entrycategory = styled.div`
  pointer-events: none;
  line-height: 1;
  margin-top: -5px;
  font-size: 0;
  font-family: "Open Sans", sans-serif;
  a {
    font-size: 0.8rem;
    line-height: 26px;
    padding: 0 15px;
    color: #fff;
    border-radius: 15px;
    text-transform: uppercase;
    pointer-events: auto;
    display: inline-block;
    vertical-align: middle;
    margin-top: 5px;
    margin-right: 5px;
    transition: all 0.15s ease-in-out;
    background-color: #7cb342;
  }
`;
const EntryTitle = styled.h3`
  margin-top: 10px;
  line-height: 1.31;
  font-size: 2.2rem;
  color: #333;
  @media (max-width: 768px) {
    font-size: 1.4rem;
  }
  a {
    &:hover {
      color: #098da3;
    }
  }
`;
const EntryMeta = styled.div`
  margin-top: 10px;
`;
const MetaItem = styled.span`
  height: 30px;
  span {
    color: rgba(74, 74, 74, 1);
    height: 30px;
    font-size: 0.9rem;
    line-height: 1.3;
    font-weight: 500;
  }
`;
const MetaComent = styled.span`
  margin: 0 0 0 10px;
  a {
    height: 30px;
    color: rgba(74, 74, 74, 1);
    font-size: 0.9rem;
    line-height: 1.3;
    font-weight: 500;
    &:hover {
      color: #098da3;
    }
  }
  &:before {
    content: "";
    width: 1px;
    height: 17px;
    display: inline-block;
    background: rgba(74, 74, 74, 0.25);
    vertical-align: middle;
    margin-right: 15px;
    margin-top: -1px;
    margin-right: 10px;
  }
`;

const BlogContent = props => {
  return (
    <TravelPost>
      <EntryHeader>
        <Entrycategory>
          <Link to="/">{props.content.location}</Link>
        </Entrycategory>
        <EntryTitle>{props.content.title}</EntryTitle>
        <EntryMeta>
          <MetaItem>
            <span>1 week a go</span>
          </MetaItem>
          <MetaComent>
            <Link to="#">3 min read</Link>
          </MetaComent>
          <MetaComent>
            <Link to="/">Add comment</Link>
          </MetaComent>
        </EntryMeta>
      </EntryHeader>
    </TravelPost>
  );
};

export default BlogContent;
