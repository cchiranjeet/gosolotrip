import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const TravelCatogery = styled.div`
  background: #ffffff;
  padding: 25px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.2);
  margin-bottom: 50px;
`;
const WidgetTitle = styled.h4`
  text-transform: uppercase;
  font-size: 1rem;
  line-height: 1.3;
  margin-bottom: 20px;
  text-align: left;
  color: #333333;
`;
const ListContainer = styled.ul`
  margin: 0;
  padding: 0;
  text-align: left;
`;
const CatItem = styled.li`
  margin-bottom: 7px;
  position: relative;
  line-height: 1.5;
  font-weight: 700;
  a {
    font-size: 0.9rem;
    color: #333333;
  }
`;
const Label = styled.span`
  color: #333333;
`;
const Count = styled.span`
  position: absolute;
  right: 0;
  top: 1px;
  height: 21px;
  border-radius: 13px;
  text-align: center;
  color: #fff;
  font-size: 0.8rem;
  font-family: "Open Sans";
  font-weight: 400;
  line-height: 21px;
  padding: 0 10px;
  min-width: 30px;
  &.countItem-1 {
    background: #d32f2f;
  }
  &.countItem-2 {
    background: #ef6c00;
  }
  &.countItem-3 {
    background: #7cb342;
  }
  &.countItem-4 {
    background: #03a9f4;
  }
  &.countItem-5 {
    background: #ab47bc;
  }
`;
const DestinationTile = props => {
  return (
    <TravelCatogery>
      <WidgetTitle>Destinations</WidgetTitle>
      <ListContainer>
        <CatItem>
          <Link to="">
            <Label>Himachal Pradesh</Label>
            <Count className="countItem-1">8</Count>
          </Link>
        </CatItem>
        <CatItem>
          <Link to="">
            <Label>Rajasthan</Label>
            <Count className="countItem-2">6</Count>
          </Link>
        </CatItem>
        <CatItem>
          <Link to="">
            <Label>Uttarakhand</Label>
            <Count className="countItem-3">8</Count>
          </Link>
        </CatItem>
        <CatItem>
          <Link to="">
            <Label>South India</Label>
            <Count className="countItem-4">6</Count>
          </Link>
        </CatItem>
        <CatItem>
          <Link to="">
            <Label>Goa</Label>
            <Count className="countItem-5">8</Count>
          </Link>
        </CatItem>
      </ListContainer>
    </TravelCatogery>
  );
};

export default DestinationTile;
