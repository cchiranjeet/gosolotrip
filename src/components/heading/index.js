import React from "react";
import styled from "styled-components";
const SectionTitle = styled.h3`
  text-transform: uppercase;
  position: relative;
  text-align: center;
  margin-bottom: 20px;
  width: 100%;

  &:after {
    content: "";
    width: 100%;
    height: 1px;
    display: block;
    top: 50%;
    margin-top: -1px;
    position: absolute;
    background: rgba(51, 51, 51, 0.1);
  }
  span {
    line-height: 50px;
    padding: 0 20px;
    font-size: 1rem;
    font-weight: 700;
    position: relative;
    z-index: 1;
    height: 50px;
    display: inline-block;
    background: #fff;
  }
`;
const SectionHeading = props => {
  return (
    <SectionTitle>
      <span>{props.children}</span>
    </SectionTitle>
  );
};

export default SectionHeading;
