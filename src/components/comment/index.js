/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import styled from "styled-components";
import Surbhi from "../../assets/img/surbhi.jpg";
import Photo from "../../assets/img/photo.jpg";
import Heading from "../../components/heading";
import CommentForm from "../../components/commentForm";
import Textarea from "../../components/form/textarea";

const CommentList = styled.div`
  .comment-body {
    margin-bottom: 40px;
    margin-top: 25px;
  }
  .children {
    padding: 0 0 0 85px;
  }
  .comment-author img {
    float: left;
    margin-right: 25px;
    & .vcard {
      font-size: 2rem;
    }
  }
  .fn {
    display: block;
    line-height: 1;
    font-size: 0.9rem;
    a {
      color: #333333;
    }
  }
  .says {
    display: none;
  }
  .comment-metadata {
    a {
      font-family: "Open Sans";
      font-weight: 400;
      color: rgba(74, 74, 74, 1);
      font-size: 0.7rem;
    }
  }
  .comment-content {
    font-size: 0.8rem;
    line-height: 1.5;
    margin-left: 85px;
    font-family: "Open Sans";
    font-weight: 400;
    p {
      margin-bottom: 10px;
    }
  }
  .Post {
    text-align: right;
    a {
      border: 1px solid rgba(51, 51, 51, 0.1);
      background: #098da3;
      color: #fff;
      font-family: "Open Sans";
      font-weight: 600;
      margin-top: 0;
      margin-left: 85px;
      display: inline-block;
      font-size: 0.8rem;
      padding: 3px 15px;
      border-radius: 3px;
      letter-spacing: 0.5px;
    }
  }
  .comment-reply-link {
    border: 1px solid rgba(51, 51, 51, 0.1);
    background: transparent;
    color: #098da3;
    font-family: "Open Sans";
    font-weight: 400;
    margin-top: 0;
    margin-left: 85px;
    display: inline-block;
    font-size: 0.8rem;
    padding: 3px 15px;
    border-radius: 3px;
    letter-spacing: 0.5px;
  }
  .commentForm {
    margin-top: 20px;
    margin-bottom: 40px;
    .cancelReply {
      border-left: 1px solid #ccc;
      margin-left: 20px;
      height: 20px;
      vertical-align: middle;
      padding: 0 0 0 20px;
      line-height: 1;
      font-size: 0.8rem;
      color: #00b7b7;
      cursor: pointer;
    }
  }
`;
class Comment extends React.Component {
  constructor(props) {
    super(props);
    this.state = { email: "", showComentForm: false };
  }
  openRespond = e => {
    e.preventDefault();
    this.setState({
      showComentForm: true
    });
  };
  cancelRespond = e => {
    e.preventDefault();
    this.setState({
      showComentForm: false
    });
  };
  render() {
    return (
      <CommentList>
        <Textarea height="70px" placeholder="Add comment" resize />
        <div className="Post">
          <a rel="nofollow" className="comment-reply-link" href="">
            POST
          </a>
        </div>
        <ul className="comment-list">
          <li>
            <article className="comment-body">
              <footer className="comment-meta">
                <div className="comment-author vcard">
                  <img alt="" src={Surbhi} height={60} width={60} />
                  <b className="fn">Lindsey Hart</b>{" "}
                  <span className="says">says:</span>{" "}
                </div>
                {/* .comment-author */}
                <div className="comment-metadata">
                  <a href="">
                    <time>March 9, 2018 at 4:26 pm</time>
                  </a>
                </div>
                {/* .comment-metadata */}
              </footer>
              {/* .comment-meta */}
              <div className="comment-content">
                <p>
                  Authoritatively exploit robust e-markets vis-a-vis B2C testing
                  procedures. Quickly streamline progressive internal or.
                </p>
              </div>
              {/* .comment-content */}
              <div className="reply" onClick={this.openRespond}>
                <a rel="nofollow" className="comment-reply-link" href="">
                  Reply
                </a>
              </div>{" "}
            </article>
            {this.state.showComentForm && (
              <div className="commentForm">
                <Heading>
                  LEAVE A REPLY
                  <span className="cancelReply" onClick={this.cancelRespond}>
                    CANCEL REPLY
                  </span>
                </Heading>
                <CommentForm />
              </div>
            )}
            {/* .comment-body */}
            <ul className="children">
              <li>
                <article id="div-comment-23" className="comment-body">
                  <footer className="comment-meta">
                    <div className="comment-author vcard">
                      <img alt="" src={Surbhi} height={60} width={60} />{" "}
                      <b className="fn">
                        <a href="" rel="external nofollow ugc" className="url">
                          Elizabeth Murphy
                        </a>
                      </b>
                      <span className="says">says:</span>
                    </div>
                    {/* .comment-author */}
                    <div className="comment-metadata">
                      <a href="">
                        <time>March 27, 2018 at 7:01 am </time>
                      </a>
                    </div>
                    {/* .comment-metadata */}
                  </footer>
                  {/* .comment-meta */}
                  <div className="comment-content">
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                      ullamco .
                    </p>
                  </div>
                  {/* .comment-content */}
                  <div className="reply">
                    <a rel="nofollow" className="comment-reply-link" href="">
                      Reply
                    </a>
                  </div>
                </article>
              </li>
              {/* #comment-## */}
            </ul>
            {/* .children */}
          </li>
          {/* #comment-## */}
          <li className="comment">
            <article className="comment-body">
              <footer className="comment-meta">
                <div className="comment-author vcard">
                  <img
                    alt=""
                    src={Surbhi}
                    className="avatar avatar-60 photo"
                    height={60}
                    width={60}
                  />
                  <b className="fn">
                    <a href="" className="url">
                      Elizabeth Murphy
                    </a>
                  </b>
                  <span className="says">says:</span>
                </div>
                {/* .comment-author */}
                <div className="comment-metadata">
                  <a href="">
                    <time>March 9, 2018 at 4:32 pm </time>
                  </a>
                </div>
                {/* .comment-metadata */}
              </footer>
              {/* .comment-meta */}
              <div className="comment-content">
                <p>
                  Uniquely iterate future-proof information and cross functional
                  internal or “organic” sources. Distinctively formulate.
                </p>
              </div>
              {/* .comment-content */}
              <div className="reply">
                <a rel="nofollow" className="comment-reply-link" href="">
                  Reply
                </a>
              </div>
            </article>
            {/* .comment-body */}
            <ul className="children">
              <li>
                <article className="comment-body">
                  <footer className="comment-meta">
                    <div className="comment-author vcard">
                      <img alt="" src={Photo} height={60} width={60} />
                      <b className="fn">George Barnett</b>
                      <span className="says">says:</span>
                    </div>
                    {/* .comment-author */}
                    <div className="comment-metadata">
                      <a href="">
                        <time>March 27, 2018 at 7:05 am </time>
                      </a>
                    </div>
                    {/* .comment-metadata */}
                  </footer>
                  {/* .comment-meta */}
                  <div className="comment-content">
                    <p>
                      Excepteur sint occaecat cupidatat non proident, sunt in
                      culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                  </div>
                  {/* .comment-content */}
                  <div className="reply">
                    <a rel="nofollow" className="comment-reply-link" href="">
                      Reply
                    </a>
                  </div>
                </article>
                {/* .comment-body */}
              </li>
              {/* #comment-## */}
            </ul>
            {/* .children */}
          </li>
          {/* #comment-## */}
        </ul>
      </CommentList>
    );
  }
}

export default Comment;
