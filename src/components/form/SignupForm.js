import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import * as firebase from "firebase";
import "firebase/auth";
import InputBox from "./input";
import styled from "styled-components";
import Button from "../button/mediumButton";
import { GoogleLogin } from "react-google-login";
import FacebookLogin from "react-facebook-login";
import FacebookICon from "../../assets/img/facebook.svg";
import GoogleICon from "../../assets/img/google.svg";
import { isAppLoaded, Initialize } from "../../index";

const InputWrapper = styled.div`
  margin-bottom: 25px;
  position: relative;
  .errorMsg {
    font-size: 12px;
    line-height: 0;
    position: absolute;
    bottom: -11px;
    color: red;
  }
`;
const SocialLogin = styled.div`
  display: flex;
  align-items: center;
  .google {
    background-color: #d34332 !important;
    border: 1px solid #d34332;
    background-image: url(${GoogleICon});
    border-radius: 3px;
    width: 40px;
    height: 40px;
    background-size: 40px;
    background-repeat: no-repeat;
    background-position: center center;
    margin-right: 15px;
  }
  .facebook {
    background-color: #3b5998 !important;
    border: 1px solid #3b5998;
    background-image: url(${FacebookICon});
    border-radius: 3px;
    width: 40px;
    height: 40px;
    background-size: 24px;
    background-repeat: no-repeat;
    background-position: center center;
  }
`;
const ServerError = styled.div`
  text-align: center;
  margin-top: 20px;
  font-size: 12px;
  color: red;
  line-height: 16px;
  font-family: "Open Sans";
  font-weight: 400;
`;

class FormSignup extends Component {
  constructor() {
    super();
    this.state = {
      message: "",
      fields: {
        username: "",
        emailid: "",
        password: ""
      },
      errors: {},
      loading: false,
      googleLoading: false
    };
  }
  componentDidMount() {
    if (!isAppLoaded()) {
      Initialize();
    }
  }
  handleChange = e => {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  };

  submituserRegistrationForm = e => {
    e.preventDefault();
    this.validateForm(() => {
      if (
        Object.keys(this.state.errors).filter(
          x => this.state.errors[x] !== undefined
        ).length === 0
      ) {
        this.setState({ loading: true });

        firebase
          .auth()
          .createUserWithEmailAndPassword(
            this.state.fields.emailid,
            this.state.fields.password
          )
          .then(() => {
            this.props.setLoggedIn(true);
          })
          .catch(error => {
            console.log(error);
            this.setState({ loading: false, message: error.message });
          });

        firebase.auth().onAuthStateChanged(user => {
          if (user) {
            this.setState({ loading: false });
            localStorage.setItem("user", JSON.stringify(user));
            user
              .updateProfile({ displayName: this.state.fields.username })
              .then(() => {
                const nextUser = firebase.auth().currentUser;
                localStorage.setItem("user", JSON.stringify(nextUser));
              });
            this.props.closeModal();
          } else {
            this.props.setLoggedIn(false);
            localStorage.setItem("user", null);
          }
        });
      }
    });
  };

  validateUserName = cb => {
    const {
      fields: { username }
    } = this.state;

    if (username.length !== 0) {
      if (!username.match(/^[a-zA-Z ]*$/)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: "*Please enter alphabet characters only."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            username: "*Please enter your username"
          }
        },
        cb
      );
    }
  };

  validateEmailID = cb => {
    const {
      fields: { emailid }
    } = this.state;

    if (emailid.length !== 0) {
      const pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(emailid)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: "*Please enter valid email-ID."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            emailid: "*Please enter your email-ID"
          }
        },
        cb
      );
    }
  };

  validatePassword = cb => {
    const {
      fields: { password }
    } = this.state;
    if (password.length === 0) {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            password: "*Please enter your Password"
          }
        },
        cb
      );
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            password: undefined
          }
        },
        cb
      );
    }
  };

  validateForm = cb => {
    this.validateUserName(() =>
      this.validateEmailID(() => this.validatePassword(cb))
    );
  };

  render() {
    const responseGoogle = response => {
      console.log(response);
    };
    const responseFacebook = response => {
      console.log(response);
    };
    return (
      <form
        method="post"
        name="userRegistrationForm"
        onSubmit={this.submituserRegistrationForm}
      >
        <InputWrapper>
          <InputBox
            type="text"
            name="username"
            placeholder="Full Name"
            onChange={this.handleChange}
          />
          <span className="errorMsg">{this.state.errors.username}</span>
        </InputWrapper>

        <InputWrapper>
          <InputBox
            type="text"
            name="emailid"
            placeholder="Email"
            onChange={this.handleChange}
          />
          <span className="errorMsg">{this.state.errors.emailid}</span>
        </InputWrapper>

        <InputWrapper>
          <InputBox
            type="password"
            name="password"
            placeholder="Create Password"
            onChange={this.handleChange}
          />
          <span className="errorMsg">{this.state.errors.password}</span>
        </InputWrapper>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginTop: "30px"
          }}
        >
          <Button type="submit" backgroundColor="#098DA3" textColor="#fff">
            {this.state.loading ? "Signing up..." : "Sign up"}
          </Button>
          <SocialLogin>
            <GoogleLogin
              clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
              onSuccess={responseGoogle}
              onFailure={responseGoogle}
              className="google"
              buttonText=""
              icon={false}
              theme="light"
              cookiePolicy={"single_host_origin"}
            />
            <FacebookLogin
              appId="1088597931155576"
              autoLoad={false}
              cssClass="facebook"
              fields="name,email,picture"
              textButton=""
              callback={responseFacebook}
            />
          </SocialLogin>
        </div>
        <ServerError>{this.state.message}</ServerError>
      </form>
    );
  }
}

export default withRouter(FormSignup);
