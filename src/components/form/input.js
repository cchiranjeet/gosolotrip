import React from "react";
import styled from "styled-components";

const InputWrapper = styled.div`
  margin-bottom: 15px;
`;
const Input = styled.input`
  padding: 5px 10px;
  font-size: 0.9rem;
  border: 1px solid rgba(0, 0, 0, 0.1);
  outline: 0;
  line-height: 1;
  width: 100%;
  margin: 0;
  height: 40px;
  vertical-align: baseline;
  border-radius: 3px;
  background: 0 0;
  overflow: visible;
`;
const Label = styled.label`
  font-size: 0.9rem;
  margin-bottom: 5px;
  line-height: 1.3;
  display: block;
  font-family: "Open Sans";
  font-weight: 400;
`;

const InputBox = props => {
  return (
    <InputWrapper>
      <Label htmlFor={props.htmlFor}>{props.label}</Label>{" "}
      <Input
        name={props.name}
        type={props.type}
        size={30}
        maxLength={100}
        placeholder={props.placeholder}
        onChange={props.onChange}
      />
    </InputWrapper>
  );
};
export default InputBox;
