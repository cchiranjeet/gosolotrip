import React from "react";
import styled from "styled-components";

const TextAreaWrapper = styled.div``;
const Label = styled.label`
  font-size: 0.9rem;
  margin-bottom: 5px;
  line-height: 1.3;
  display: block;
  font-family: "Open Sans";
  font-weight: 400;
`;
const TextArea = styled.textarea`
  overflow: auto;
  resize: ${p => (p.resize ? "none" : "vertical")};
  border: 1px solid rgba(0, 0, 0, 0.1);
  outline: 0;
  line-height: 1;
  height: ${p => (p.height ? p.height : "150px")};
  display: block;
  width: 100%;
  margin-bottom: 15px;
  border-radius: 3px;
  padding: 10px;
  font-size: 0.9rem;
`;

const TextAreaBox = props => {
  return (
    <TextAreaWrapper>
      <Label htmlFor={props.htmlFor}>{props.label}</Label>{" "}
      <TextArea
        name={props.name}
        maxLength={65525}
        defaultValue={""}
        placeholder={props.placeholder}
        resize={props.resize}
        height={props.height}
        onChange={props.onChange}
      />
    </TextAreaWrapper>
  );
};
export default TextAreaBox;
