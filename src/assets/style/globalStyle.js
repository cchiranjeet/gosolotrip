import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
  @import url('https://d1azc1qln24ryf.cloudfront.net/114779/Socicon/style-cf.css?u8vidh');
  @import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap');
  @import url('https://fonts.googleapis.com/css?family=Quicksand:300,400,500,600,700&display=swap');
  *{
    margin:0;
    padding:0;
    box-sizing:border-box;
    -webkit-font-smoothing: antialiased;
  }
  ol, ul {
    list-style: none;
  }
  a{
    text-decoration:none;
    color:#fff;
  }
  img{
    -webkit-backface-visibility: hidden;
    -ms-transform: translateZ(0);
    -webkit-transform: translateZ(0);
    transform: translateZ(0);
    image-rendering: optimizeSpeed;
    image-rendering: -moz-crisp-edges;
    image-rendering: -o-crisp-edges;
    image-rendering: -webkit-optimize-contrast;
    image-rendering: optimize-contrast;
    -ms-interpolation-mode: nearest-neighbor;
    max-width: 100%;
    height: auto;
  }
  html {
    font-family: sans-serif;
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    -ms-overflow-style: scrollbar;
    -webkit-tap-highlight-color: transparent;
  }
  body {
    font-family: 'Quicksand', sans-serif;
    background:#fff;
    color:#333;
    line-height: 1.75;
    font-weight: 400;
    overflow-x:hidden;
  }
  .gm-style{
    font :unset;
  }
  .gm-style .gm-style-iw-c{
    padding:0;
  }
  .gm-style .gm-style-iw-d{
    overflow:hidden!important;
    width:300px;
  }
  .gm-style img {
    max-width: 325px; 
  }
  .gm-style .gm-style-iw{
    border-radius:4px !important;
  }
  .gm-ui-hover-effect {
    /* opacity: .6; */
    background: #fff !important;
    border-radius: 100px;
    right: 6px !important;
    top: 5px!important;
  }
  .fb-comments{
    width:100%;
    iframe{
      width:100% !important;

    }
  }
`;
export default GlobalStyles;
